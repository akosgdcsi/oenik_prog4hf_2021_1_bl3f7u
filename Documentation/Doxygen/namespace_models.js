var namespace_models =
[
    [ "Planet", "class_models_1_1_planet.html", "class_models_1_1_planet" ],
    [ "PlanetTypeGrouped", "class_models_1_1_planet_type_grouped.html", "class_models_1_1_planet_type_grouped" ],
    [ "PopulationInSec", "class_models_1_1_population_in_sec.html", "class_models_1_1_population_in_sec" ],
    [ "Star", "class_models_1_1_star.html", "class_models_1_1_star" ],
    [ "Stats", "class_models_1_1_stats.html", "class_models_1_1_stats" ],
    [ "System", "class_models_1_1_system.html", "class_models_1_1_system" ],
    [ "PlanetType", "namespace_models.html#ade608462512ec967a1bc6da9a5aba002", [
      [ "Miniterran", "namespace_models.html#ade608462512ec967a1bc6da9a5aba002a49029d11b6c4638610a6f9387c9fa730", null ],
      [ "Subterran", "namespace_models.html#ade608462512ec967a1bc6da9a5aba002a6cd0192d80181f773b6692a860865b52", null ],
      [ "Terran", "namespace_models.html#ade608462512ec967a1bc6da9a5aba002a0acf621f59813ec762188e5ea36fb307", null ],
      [ "Superterran", "namespace_models.html#ade608462512ec967a1bc6da9a5aba002a3d853dbd434f11121ba0e97db7e38ff6", null ],
      [ "Neptunian", "namespace_models.html#ade608462512ec967a1bc6da9a5aba002a0e0888027dac08a394756991a7187e93", null ],
      [ "Jovian", "namespace_models.html#ade608462512ec967a1bc6da9a5aba002a5f44c1e9d6abcad9bf9665d8a29cdefb", null ]
    ] ],
    [ "StarType", "namespace_models.html#a5a6c86c61774ee98b18885c07defcada", [
      [ "Blue_Stars", "namespace_models.html#a5a6c86c61774ee98b18885c07defcadaa320ded43bca3fd9ba1e20c9bcf6e8950", null ],
      [ "Yellow_Dwarfs", "namespace_models.html#a5a6c86c61774ee98b18885c07defcadaaff692982fcc99a82098f4fa266d7d9d5", null ],
      [ "Orange_Dwarfs", "namespace_models.html#a5a6c86c61774ee98b18885c07defcadaa3e6b1747e3ba45ad90c04d9acc64fda8", null ],
      [ "Red_Dwarfs", "namespace_models.html#a5a6c86c61774ee98b18885c07defcadaaf94cdb59253d2d82353a266ee4b8b95d", null ],
      [ "White_Dwarfs", "namespace_models.html#a5a6c86c61774ee98b18885c07defcadaad801f532ba2b5905bccbb42dc0337cba", null ],
      [ "Red_Giants", "namespace_models.html#a5a6c86c61774ee98b18885c07defcadaa64a8dd2e113929fea8fe636c763f06de", null ],
      [ "Blue_Giants", "namespace_models.html#a5a6c86c61774ee98b18885c07defcadaa1f778f9f924e6d1ae1497e43545df3c9", null ]
    ] ]
];