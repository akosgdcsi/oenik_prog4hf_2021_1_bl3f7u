var class_logic_1_1_planet_logic =
[
    [ "PlanetLogic", "class_logic_1_1_planet_logic.html#ac8f4da7d6a9ae8488607de8d034bc34d", null ],
    [ "AddPlanet", "class_logic_1_1_planet_logic.html#a764f508fe2db18d5665db69927d6b4b2", null ],
    [ "DeletePlanet", "class_logic_1_1_planet_logic.html#aaa30a0c9b546151f9b370b3b39d5ed4f", null ],
    [ "GetAllPlanet", "class_logic_1_1_planet_logic.html#a1752b456bd9d014b40528baf4dd57c0c", null ],
    [ "GetPlanet", "class_logic_1_1_planet_logic.html#a93ca79fbfa4b8aae31a78e357a87fce8", null ],
    [ "PlanetToStar", "class_logic_1_1_planet_logic.html#a4085a28f0c4dfe1608b16a691e67173c", null ],
    [ "UpdatePlanet", "class_logic_1_1_planet_logic.html#a95945c36dafde29bf227de914b7a8f18", null ]
];