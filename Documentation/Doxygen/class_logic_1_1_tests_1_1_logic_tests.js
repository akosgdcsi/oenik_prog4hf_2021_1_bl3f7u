var class_logic_1_1_tests_1_1_logic_tests =
[
    [ "TestAdd", "class_logic_1_1_tests_1_1_logic_tests.html#a8efc8d631d2ce2819c0c40818f8b6209", null ],
    [ "TestDelete", "class_logic_1_1_tests_1_1_logic_tests.html#a57ca8554fd67b5cdc660454a76d0aaef", null ],
    [ "TestGetAllPlanet", "class_logic_1_1_tests_1_1_logic_tests.html#a54bc6a1de2180e1152e54ff11778c2d6", null ],
    [ "TestGetSystem", "class_logic_1_1_tests_1_1_logic_tests.html#a1c1eebda0b6727d09612f5fb59dbf810", null ],
    [ "TestPlanetTypeGrouped", "class_logic_1_1_tests_1_1_logic_tests.html#a137aa1014a93b9cc09e0ab9805d0e53d", null ],
    [ "TestPopulationInSectors", "class_logic_1_1_tests_1_1_logic_tests.html#aba260c170e7e5435d8439f70eeee7f34", null ],
    [ "TestStarsWithLife", "class_logic_1_1_tests_1_1_logic_tests.html#ad2074b3928666f73571c02858dab3f11", null ],
    [ "TestUpdate", "class_logic_1_1_tests_1_1_logic_tests.html#a5f7972c5d9cbff30215e856f86d84ed7", null ]
];