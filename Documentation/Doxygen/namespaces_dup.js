var namespaces_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", "namespace_asp_net_core" ],
    [ "Data", "namespace_data.html", "namespace_data" ],
    [ "Logic", "namespace_logic.html", "namespace_logic" ],
    [ "Models", "namespace_models.html", "namespace_models" ],
    [ "MVC", "namespace_m_v_c.html", "namespace_m_v_c" ],
    [ "Repository", "namespace_repository.html", "namespace_repository" ]
];