var class_logic_1_1_system_logic =
[
    [ "SystemLogic", "class_logic_1_1_system_logic.html#a0f79042dee2c58f88c025e2dfdf306ea", null ],
    [ "AddSystem", "class_logic_1_1_system_logic.html#a360ff073fa45539a2ac519967028931b", null ],
    [ "DeleteSystem", "class_logic_1_1_system_logic.html#aebdadd2863afce05cc7f5e1a0e43d570", null ],
    [ "GetAllSystem", "class_logic_1_1_system_logic.html#a6b7671b3311e7432d84daf7666c5aef9", null ],
    [ "GetStar", "class_logic_1_1_system_logic.html#a2d6130085a7df7f235b67f2917fb6cf2", null ],
    [ "GetSystem", "class_logic_1_1_system_logic.html#a719f123d115b213940498315890bb30d", null ],
    [ "UpdateSystem", "class_logic_1_1_system_logic.html#a8b73b2f5459b2a2cd279a58cc1f1c9f7", null ]
];