var searchData=
[
  ['save_205',['Save',['../interface_repository_1_1_i_repository.html#a0d7eed16cc94bb001dd1e728ecfb171c',1,'Repository.IRepository.Save()'],['../class_repository_1_1_planet_repository.html#ac9d4744bf587143449afbf80adce3e12',1,'Repository.PlanetRepository.Save()'],['../class_repository_1_1_star_repository.html#aeedf59be6a1f42e86874079cba722e8e',1,'Repository.StarRepository.Save()'],['../class_repository_1_1_system_repository.html#aa3235234463d1aeae1e32c22974617d4',1,'Repository.SystemRepository.Save()']]],
  ['solarcontext_206',['SolarContext',['../class_data_1_1_solar_context.html#a1e0d93b74d7b02e9f27e490ca5000585',1,'Data.SolarContext.SolarContext(DbContextOptions&lt; SolarContext &gt; opt)'],['../class_data_1_1_solar_context.html#ab421e33ae370f4dfd5ddd383604eeb0b',1,'Data.SolarContext.SolarContext()']]],
  ['starlogic_207',['StarLogic',['../class_logic_1_1_star_logic.html#adbbaf4c15445700d8782532944f7ec4c',1,'Logic::StarLogic']]],
  ['starswithlife_208',['StarsWithLife',['../class_logic_1_1_stats_logic.html#a938c4f27bef3a82f455fec2263c87adb',1,'Logic::StatsLogic']]],
  ['startoplanetlist_209',['StarToPlanetList',['../class_m_v_c_1_1_controllers_1_1_home_controller.html#ac94fe2e7b416f596b62f9c73853e10f8',1,'MVC::Controllers::HomeController']]],
  ['startosystem_210',['StarToSystem',['../class_logic_1_1_star_logic.html#a6a45091190effc1f8ccf13f9cceb03ae',1,'Logic::StarLogic']]],
  ['stats_211',['Stats',['../class_m_v_c_1_1_controllers_1_1_home_controller.html#ae94ce1e8987d13d587354e3a2c70270e',1,'MVC::Controllers::HomeController']]],
  ['statslogic_212',['StatsLogic',['../class_logic_1_1_stats_logic.html#a7cda1751b9ba030dd40ff4a8dbc93683',1,'Logic::StatsLogic']]],
  ['systemlogic_213',['SystemLogic',['../class_logic_1_1_system_logic.html#a0f79042dee2c58f88c025e2dfdf306ea',1,'Logic::SystemLogic']]],
  ['systemtostarlist_214',['SystemToStarList',['../class_m_v_c_1_1_controllers_1_1_home_controller.html#ab930a01b28d78efc17f1119df5ae95cb',1,'MVC::Controllers::HomeController']]]
];
