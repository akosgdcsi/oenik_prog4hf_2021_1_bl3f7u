var searchData=
[
  ['sectorname_252',['SectorName',['../class_models_1_1_system.html#ab6a26b22a1783d7019b400d89c2c1691',1,'Models::System']]],
  ['sectortype_253',['SectorType',['../class_models_1_1_population_in_sec.html#ac85d7b6fbcc964be77406465501e43da',1,'Models::PopulationInSec']]],
  ['star_254',['Star',['../class_models_1_1_planet.html#a30d71456f4d2e2ad905c223554d0bb89',1,'Models::Planet']]],
  ['starid_255',['StarID',['../class_models_1_1_planet.html#a457618de2765f53cfd370788be117804',1,'Models.Planet.StarID()'],['../class_models_1_1_star.html#ab4effdf525e1196df9d9097dc117b8a8',1,'Models.Star.StarID()']]],
  ['stars_256',['Stars',['../class_data_1_1_solar_context.html#ad2fdfa7ace024e2e2fe3984fa5eb58f0',1,'Data.SolarContext.Stars()'],['../class_models_1_1_system.html#ad745a79adc1534354db8b38df94ffc3f',1,'Models.System.Stars()']]],
  ['starswithlife_257',['StarsWithLife',['../class_models_1_1_stats.html#afc458245e8f4659511a17b2e51c9c7b9',1,'Models::Stats']]],
  ['startype_258',['StarType',['../class_models_1_1_star.html#a583d94c65ed17771c0066dd24dbff2fa',1,'Models::Star']]],
  ['system_259',['System',['../class_models_1_1_star.html#ae93a69765a4f88f180afa109d0657aca',1,'Models::Star']]],
  ['systemid_260',['SystemID',['../class_models_1_1_star.html#a81779c0d67158ea6f6d4fe75fb225bf9',1,'Models.Star.SystemID()'],['../class_models_1_1_system.html#a7141ddddbce363818fd4c37149b30042',1,'Models.System.SystemID()']]],
  ['systems_261',['Systems',['../class_data_1_1_solar_context.html#a8100867f8fe701028ceee1efe0350fa5',1,'Data::SolarContext']]]
];
