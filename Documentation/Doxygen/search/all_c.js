var searchData=
[
  ['name_49',['Name',['../class_models_1_1_planet.html#ae0aaa0ae03c4bf7f1530f50ed312ab61',1,'Models.Planet.Name()'],['../class_models_1_1_star.html#a1a9d45908a72927ab1a996c1a53568ad',1,'Models.Star.Name()'],['../class_models_1_1_system.html#a6bf9a49d4bde5c7c07faee03a81e4422',1,'Models.System.Name()']]],
  ['neptunian_50',['Neptunian',['../namespace_models.html#ade608462512ec967a1bc6da9a5aba002a0e0888027dac08a394756991a7187e93',1,'Models']]],
  ['numberofstars_51',['NumberOfStars',['../class_models_1_1_planet_type_grouped.html#a10e906d6f8de3e959d6f2bb526d7a515',1,'Models::PlanetTypeGrouped']]]
];
