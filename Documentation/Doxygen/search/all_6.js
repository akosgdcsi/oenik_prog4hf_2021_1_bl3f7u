var searchData=
[
  ['getallplanet_18',['GetAllPlanet',['../class_logic_1_1_planet_logic.html#a1752b456bd9d014b40528baf4dd57c0c',1,'Logic::PlanetLogic']]],
  ['getallstar_19',['GetAllStar',['../class_logic_1_1_star_logic.html#a8a3738447f3baa7d1cf70c587e41db90',1,'Logic::StarLogic']]],
  ['getallsystem_20',['GetAllSystem',['../class_logic_1_1_system_logic.html#a6b7671b3311e7432d84daf7666c5aef9',1,'Logic::SystemLogic']]],
  ['gethashcode_21',['GetHashCode',['../class_models_1_1_planet.html#ac764ceee134c6a983d04a662193fc359',1,'Models.Planet.GetHashCode()'],['../class_models_1_1_star.html#a23822620dfe5731463cf4b9fffdfa84d',1,'Models.Star.GetHashCode()'],['../class_models_1_1_system.html#a6d92c586d9335d1c87ecb333ce935114',1,'Models.System.GetHashCode()']]],
  ['getplanet_22',['GetPlanet',['../class_logic_1_1_planet_logic.html#a93ca79fbfa4b8aae31a78e357a87fce8',1,'Logic.PlanetLogic.GetPlanet()'],['../class_logic_1_1_star_logic.html#a2052d621e3dfc2bd44bc559f8e08f8b7',1,'Logic.StarLogic.GetPlanet(string id)']]],
  ['getstar_23',['GetStar',['../class_logic_1_1_star_logic.html#a3f4805ea429d976209d50922757dd542',1,'Logic.StarLogic.GetStar()'],['../class_logic_1_1_system_logic.html#a2d6130085a7df7f235b67f2917fb6cf2',1,'Logic.SystemLogic.GetStar(string id)']]],
  ['getsystem_24',['GetSystem',['../class_logic_1_1_system_logic.html#a719f123d115b213940498315890bb30d',1,'Logic::SystemLogic']]]
];
