var hierarchy =
[
    [ "Controller", null, [
      [ "MVC.Controllers.HomeController", "class_m_v_c_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "Data.SolarContext", "class_data_1_1_solar_context.html", null ]
    ] ],
    [ "Repository.IRepository< T >", "interface_repository_1_1_i_repository.html", null ],
    [ "Repository.IRepository< Models.Models.System >", "interface_repository_1_1_i_repository.html", null ],
    [ "Repository.IRepository< Models.System >", "interface_repository_1_1_i_repository.html", [
      [ "Repository.SystemRepository", "class_repository_1_1_system_repository.html", null ]
    ] ],
    [ "Repository.IRepository< Models.Planet >", "interface_repository_1_1_i_repository.html", null ],
    [ "Repository.IRepository< Models.Star >", "interface_repository_1_1_i_repository.html", null ],
    [ "Repository.IRepository< Planet >", "interface_repository_1_1_i_repository.html", [
      [ "Repository.PlanetRepository", "class_repository_1_1_planet_repository.html", null ]
    ] ],
    [ "Repository.IRepository< Star >", "interface_repository_1_1_i_repository.html", [
      [ "Repository.StarRepository", "class_repository_1_1_star_repository.html", null ]
    ] ],
    [ "Logic.Tests.LogicTests", "class_logic_1_1_tests_1_1_logic_tests.html", null ],
    [ "Models.Planet", "class_models_1_1_planet.html", null ],
    [ "Logic.PlanetLogic", "class_logic_1_1_planet_logic.html", null ],
    [ "Models.PlanetTypeGrouped", "class_models_1_1_planet_type_grouped.html", null ],
    [ "Models.PopulationInSec", "class_models_1_1_population_in_sec.html", null ],
    [ "MVC.Program", "class_m_v_c_1_1_program.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Home_AddPlanet", "class_asp_net_core_1_1_views___home___add_planet.html", null ],
      [ "AspNetCore.Views_Home_AddPlanet", "class_asp_net_core_1_1_views___home___add_planet.html", null ],
      [ "AspNetCore.Views_Home_AddStar", "class_asp_net_core_1_1_views___home___add_star.html", null ],
      [ "AspNetCore.Views_Home_AddStar", "class_asp_net_core_1_1_views___home___add_star.html", null ],
      [ "AspNetCore.Views_Home_AddSystem", "class_asp_net_core_1_1_views___home___add_system.html", null ],
      [ "AspNetCore.Views_Home_AddSystem", "class_asp_net_core_1_1_views___home___add_system.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Stats", "class_asp_net_core_1_1_views___home___stats.html", null ],
      [ "AspNetCore.Views_Home_Stats", "class_asp_net_core_1_1_views___home___stats.html", null ],
      [ "AspNetCore.Views_Home_UpdatePlanet", "class_asp_net_core_1_1_views___home___update_planet.html", null ],
      [ "AspNetCore.Views_Home_UpdatePlanet", "class_asp_net_core_1_1_views___home___update_planet.html", null ],
      [ "AspNetCore.Views_Home_UpdateStar", "class_asp_net_core_1_1_views___home___update_star.html", null ],
      [ "AspNetCore.Views_Home_UpdateStar", "class_asp_net_core_1_1_views___home___update_star.html", null ],
      [ "AspNetCore.Views_Home_UpdateSystem", "class_asp_net_core_1_1_views___home___update_system.html", null ],
      [ "AspNetCore.Views_Home_UpdateSystem", "class_asp_net_core_1_1_views___home___update_system.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__Viewstat", "class_asp_net_core_1_1_views___shared_____viewstat.html", null ],
      [ "AspNetCore.Views_Shared__Viewstat", "class_asp_net_core_1_1_views___shared_____viewstat.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< Models.Planet >>", null, [
      [ "AspNetCore.Views_Home_ListPlanet", "class_asp_net_core_1_1_views___home___list_planet.html", null ],
      [ "AspNetCore.Views_Home_ListPlanet", "class_asp_net_core_1_1_views___home___list_planet.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< Models.Star >>", null, [
      [ "AspNetCore.Views_Home_ListStar", "class_asp_net_core_1_1_views___home___list_star.html", null ],
      [ "AspNetCore.Views_Home_ListStar", "class_asp_net_core_1_1_views___home___list_star.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< Models.System >>", null, [
      [ "AspNetCore.Views_Home_ListSystem", "class_asp_net_core_1_1_views___home___list_system.html", null ],
      [ "AspNetCore.Views_Home_ListSystem", "class_asp_net_core_1_1_views___home___list_system.html", null ]
    ] ],
    [ "Models.Star", "class_models_1_1_star.html", null ],
    [ "Logic.StarLogic", "class_logic_1_1_star_logic.html", null ],
    [ "MVC.Startup", "class_m_v_c_1_1_startup.html", null ],
    [ "Models.Stats", "class_models_1_1_stats.html", null ],
    [ "Logic.StatsLogic", "class_logic_1_1_stats_logic.html", null ],
    [ "Models.System", "class_models_1_1_system.html", null ],
    [ "Logic.SystemLogic", "class_logic_1_1_system_logic.html", null ]
];