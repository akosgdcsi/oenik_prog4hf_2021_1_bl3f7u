var interface_repository_1_1_i_repository =
[
    [ "Add", "interface_repository_1_1_i_repository.html#ad0af6d01231158937244f69a3927811f", null ],
    [ "Delete", "interface_repository_1_1_i_repository.html#a4e2f329a9659d588cfd0a27a6939b22f", null ],
    [ "Delete", "interface_repository_1_1_i_repository.html#a49fb9cb767e6999823a6e947c8aefc3b", null ],
    [ "Read", "interface_repository_1_1_i_repository.html#adc2788a3312c13a0f682e48e77d17b25", null ],
    [ "Read", "interface_repository_1_1_i_repository.html#aa31f500981761876b003f23d27cfd0e2", null ],
    [ "Save", "interface_repository_1_1_i_repository.html#a0d7eed16cc94bb001dd1e728ecfb171c", null ],
    [ "Update", "interface_repository_1_1_i_repository.html#aeb6f29b94ba27066daa1ee9839bb2d38", null ]
];