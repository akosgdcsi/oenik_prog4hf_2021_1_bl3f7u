var class_models_1_1_star =
[
    [ "Equals", "class_models_1_1_star.html#af4fa09b1baa35bc3c74f9105752da4d6", null ],
    [ "GetHashCode", "class_models_1_1_star.html#a23822620dfe5731463cf4b9fffdfa84d", null ],
    [ "Age", "class_models_1_1_star.html#aea8d529d4aab52b7fd602cb5c9ea3dcc", null ],
    [ "Name", "class_models_1_1_star.html#a1a9d45908a72927ab1a996c1a53568ad", null ],
    [ "Planets", "class_models_1_1_star.html#a3d9c70866f881d277625112ce23c21d3", null ],
    [ "StarID", "class_models_1_1_star.html#ab4effdf525e1196df9d9097dc117b8a8", null ],
    [ "StarType", "class_models_1_1_star.html#a583d94c65ed17771c0066dd24dbff2fa", null ],
    [ "System", "class_models_1_1_star.html#ae93a69765a4f88f180afa109d0657aca", null ],
    [ "SystemID", "class_models_1_1_star.html#a81779c0d67158ea6f6d4fe75fb225bf9", null ]
];