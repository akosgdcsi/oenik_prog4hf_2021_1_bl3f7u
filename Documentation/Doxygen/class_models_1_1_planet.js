var class_models_1_1_planet =
[
    [ "Equals", "class_models_1_1_planet.html#a203b56f42ade8709630cc56b6421ab99", null ],
    [ "GetHashCode", "class_models_1_1_planet.html#ac764ceee134c6a983d04a662193fc359", null ],
    [ "Habitable", "class_models_1_1_planet.html#af9e93adb0baceffbe1b7a40375e51d63", null ],
    [ "Name", "class_models_1_1_planet.html#ae0aaa0ae03c4bf7f1530f50ed312ab61", null ],
    [ "PlanetID", "class_models_1_1_planet.html#ac94efd53b83ace9cd44a96476ef8762d", null ],
    [ "PlanetType", "class_models_1_1_planet.html#ab63bd4caf529331f2c3ffd064384a417", null ],
    [ "Population", "class_models_1_1_planet.html#aa54d3c006b5ed3aeaac0c327b745b5a4", null ],
    [ "Star", "class_models_1_1_planet.html#a30d71456f4d2e2ad905c223554d0bb89", null ],
    [ "StarID", "class_models_1_1_planet.html#a457618de2765f53cfd370788be117804", null ]
];