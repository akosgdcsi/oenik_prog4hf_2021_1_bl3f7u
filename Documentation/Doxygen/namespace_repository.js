var namespace_repository =
[
    [ "IRepository", "interface_repository_1_1_i_repository.html", "interface_repository_1_1_i_repository" ],
    [ "PlanetRepository", "class_repository_1_1_planet_repository.html", "class_repository_1_1_planet_repository" ],
    [ "StarRepository", "class_repository_1_1_star_repository.html", "class_repository_1_1_star_repository" ],
    [ "SystemRepository", "class_repository_1_1_system_repository.html", "class_repository_1_1_system_repository" ]
];