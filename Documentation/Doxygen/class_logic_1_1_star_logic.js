var class_logic_1_1_star_logic =
[
    [ "StarLogic", "class_logic_1_1_star_logic.html#adbbaf4c15445700d8782532944f7ec4c", null ],
    [ "AddStar", "class_logic_1_1_star_logic.html#ac1fa698b735ae52b29983257aa5a02b4", null ],
    [ "DeleteStar", "class_logic_1_1_star_logic.html#af2265496810ccce56c603840c89e41bf", null ],
    [ "GetAllStar", "class_logic_1_1_star_logic.html#a8a3738447f3baa7d1cf70c587e41db90", null ],
    [ "GetPlanet", "class_logic_1_1_star_logic.html#a2052d621e3dfc2bd44bc559f8e08f8b7", null ],
    [ "GetStar", "class_logic_1_1_star_logic.html#a3f4805ea429d976209d50922757dd542", null ],
    [ "StarToSystem", "class_logic_1_1_star_logic.html#a6a45091190effc1f8ccf13f9cceb03ae", null ],
    [ "UpdateStar", "class_logic_1_1_star_logic.html#a1f72477271a9f15d8b2966133ea0d26f", null ]
];