var namespace_asp_net_core =
[
    [ "Views_Home_AddPlanet", "class_asp_net_core_1_1_views___home___add_planet.html", "class_asp_net_core_1_1_views___home___add_planet" ],
    [ "Views_Home_AddStar", "class_asp_net_core_1_1_views___home___add_star.html", "class_asp_net_core_1_1_views___home___add_star" ],
    [ "Views_Home_AddSystem", "class_asp_net_core_1_1_views___home___add_system.html", "class_asp_net_core_1_1_views___home___add_system" ],
    [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
    [ "Views_Home_ListPlanet", "class_asp_net_core_1_1_views___home___list_planet.html", "class_asp_net_core_1_1_views___home___list_planet" ],
    [ "Views_Home_ListStar", "class_asp_net_core_1_1_views___home___list_star.html", "class_asp_net_core_1_1_views___home___list_star" ],
    [ "Views_Home_ListSystem", "class_asp_net_core_1_1_views___home___list_system.html", "class_asp_net_core_1_1_views___home___list_system" ],
    [ "Views_Home_Stats", "class_asp_net_core_1_1_views___home___stats.html", "class_asp_net_core_1_1_views___home___stats" ],
    [ "Views_Home_UpdatePlanet", "class_asp_net_core_1_1_views___home___update_planet.html", "class_asp_net_core_1_1_views___home___update_planet" ],
    [ "Views_Home_UpdateStar", "class_asp_net_core_1_1_views___home___update_star.html", "class_asp_net_core_1_1_views___home___update_star" ],
    [ "Views_Home_UpdateSystem", "class_asp_net_core_1_1_views___home___update_system.html", "class_asp_net_core_1_1_views___home___update_system" ],
    [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
    [ "Views_Shared__Viewstat", "class_asp_net_core_1_1_views___shared_____viewstat.html", "class_asp_net_core_1_1_views___shared_____viewstat" ]
];