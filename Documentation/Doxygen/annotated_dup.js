var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views_Home_AddPlanet", "class_asp_net_core_1_1_views___home___add_planet.html", "class_asp_net_core_1_1_views___home___add_planet" ],
      [ "Views_Home_AddStar", "class_asp_net_core_1_1_views___home___add_star.html", "class_asp_net_core_1_1_views___home___add_star" ],
      [ "Views_Home_AddSystem", "class_asp_net_core_1_1_views___home___add_system.html", "class_asp_net_core_1_1_views___home___add_system" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_ListPlanet", "class_asp_net_core_1_1_views___home___list_planet.html", "class_asp_net_core_1_1_views___home___list_planet" ],
      [ "Views_Home_ListStar", "class_asp_net_core_1_1_views___home___list_star.html", "class_asp_net_core_1_1_views___home___list_star" ],
      [ "Views_Home_ListSystem", "class_asp_net_core_1_1_views___home___list_system.html", "class_asp_net_core_1_1_views___home___list_system" ],
      [ "Views_Home_Stats", "class_asp_net_core_1_1_views___home___stats.html", "class_asp_net_core_1_1_views___home___stats" ],
      [ "Views_Home_UpdatePlanet", "class_asp_net_core_1_1_views___home___update_planet.html", "class_asp_net_core_1_1_views___home___update_planet" ],
      [ "Views_Home_UpdateStar", "class_asp_net_core_1_1_views___home___update_star.html", "class_asp_net_core_1_1_views___home___update_star" ],
      [ "Views_Home_UpdateSystem", "class_asp_net_core_1_1_views___home___update_system.html", "class_asp_net_core_1_1_views___home___update_system" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
      [ "Views_Shared__Viewstat", "class_asp_net_core_1_1_views___shared_____viewstat.html", "class_asp_net_core_1_1_views___shared_____viewstat" ]
    ] ],
    [ "Data", "namespace_data.html", [
      [ "SolarContext", "class_data_1_1_solar_context.html", "class_data_1_1_solar_context" ]
    ] ],
    [ "Logic", "namespace_logic.html", [
      [ "Tests", "namespace_logic_1_1_tests.html", [
        [ "LogicTests", "class_logic_1_1_tests_1_1_logic_tests.html", "class_logic_1_1_tests_1_1_logic_tests" ]
      ] ],
      [ "PlanetLogic", "class_logic_1_1_planet_logic.html", "class_logic_1_1_planet_logic" ],
      [ "StarLogic", "class_logic_1_1_star_logic.html", "class_logic_1_1_star_logic" ],
      [ "StatsLogic", "class_logic_1_1_stats_logic.html", "class_logic_1_1_stats_logic" ],
      [ "SystemLogic", "class_logic_1_1_system_logic.html", "class_logic_1_1_system_logic" ]
    ] ],
    [ "Models", "namespace_models.html", [
      [ "Planet", "class_models_1_1_planet.html", "class_models_1_1_planet" ],
      [ "PlanetTypeGrouped", "class_models_1_1_planet_type_grouped.html", "class_models_1_1_planet_type_grouped" ],
      [ "PopulationInSec", "class_models_1_1_population_in_sec.html", "class_models_1_1_population_in_sec" ],
      [ "Star", "class_models_1_1_star.html", "class_models_1_1_star" ],
      [ "Stats", "class_models_1_1_stats.html", "class_models_1_1_stats" ],
      [ "System", "class_models_1_1_system.html", "class_models_1_1_system" ]
    ] ],
    [ "MVC", "namespace_m_v_c.html", [
      [ "Controllers", "namespace_m_v_c_1_1_controllers.html", [
        [ "HomeController", "class_m_v_c_1_1_controllers_1_1_home_controller.html", "class_m_v_c_1_1_controllers_1_1_home_controller" ]
      ] ],
      [ "Program", "class_m_v_c_1_1_program.html", "class_m_v_c_1_1_program" ],
      [ "Startup", "class_m_v_c_1_1_startup.html", "class_m_v_c_1_1_startup" ]
    ] ],
    [ "Repository", "namespace_repository.html", [
      [ "IRepository", "interface_repository_1_1_i_repository.html", "interface_repository_1_1_i_repository" ],
      [ "PlanetRepository", "class_repository_1_1_planet_repository.html", "class_repository_1_1_planet_repository" ],
      [ "StarRepository", "class_repository_1_1_star_repository.html", "class_repository_1_1_star_repository" ],
      [ "SystemRepository", "class_repository_1_1_system_repository.html", "class_repository_1_1_system_repository" ]
    ] ]
];