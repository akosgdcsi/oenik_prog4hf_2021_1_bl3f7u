var namespace_logic =
[
    [ "Tests", "namespace_logic_1_1_tests.html", "namespace_logic_1_1_tests" ],
    [ "PlanetLogic", "class_logic_1_1_planet_logic.html", "class_logic_1_1_planet_logic" ],
    [ "StarLogic", "class_logic_1_1_star_logic.html", "class_logic_1_1_star_logic" ],
    [ "StatsLogic", "class_logic_1_1_stats_logic.html", "class_logic_1_1_stats_logic" ],
    [ "SystemLogic", "class_logic_1_1_system_logic.html", "class_logic_1_1_system_logic" ]
];