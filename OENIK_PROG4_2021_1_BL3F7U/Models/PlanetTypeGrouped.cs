﻿// <copyright file="PlanetTypeGrouped.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Models
{
    /// <summary>
    /// This is a documation for PlanetTypeGrouped file.
    /// </summary>
    public class PlanetTypeGrouped
    {
        /// <summary>
        /// Gets or sets a planet type.
        /// </summary>
        public PlanetType Type { get; set; }

        /// <summary>
        /// Gets or sets the number of stars.
        /// </summary>
        public int NumberOfStars { get; set; }
    }
}