﻿// <copyright file="Star.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace Models
{
    using global::System.Collections.Generic;
    using global::System.ComponentModel.DataAnnotations;
    using global::System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Types of Stars.
    /// </summary>
    public enum StarType
    {
        /// <summary>
        /// Represents a Blue_Stars.
        /// </summary>
        Blue_Stars,

        /// <summary>
        /// Represents a Yellow_Dwarfs.
        /// </summary>
        Yellow_Dwarfs,

        /// <summary>
        /// Represents a Orange_Dwarfs.
        /// </summary>
        Orange_Dwarfs,

        /// <summary>
        /// Represents a Red_Dwarfs.
        /// </summary>
        Red_Dwarfs,

        /// <summary>
        /// Represents a White_Dwarfs.
        /// </summary>
        White_Dwarfs,

        /// <summary>
        /// Represents a Red_Giants.
        /// </summary>
        Red_Giants,

        /// <summary>
        /// Represents a Blue_Giants.
        /// </summary>
        Blue_Giants,
    }

    /// <summary>
    /// This is a documation for Star file.
    /// </summary>
    public class Star
    {
        /// <summary>
        /// Gets or sets a star identification.
        /// </summary>
        [Key]
        public string StarID { get; set; }

        /// <summary>
        /// Gets or sets a star name.
        /// </summary>
        [StringLength(200)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a star type.
        /// </summary>
        public StarType StarType { get; set; }

        /// <summary>
        /// Gets or sets a stars age.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets a star's system identification.
        /// </summary>
        public string SystemID { get; set; }

        /// <summary>
        /// Gets or sets a virtual notmapped .
        /// </summary>
        [NotMapped]
        public virtual StarSystem System { get; set; }

        /// <summary>
        /// Gets or sets a virtual icollection .
        /// </summary>
        public virtual ICollection<Planet> Planets { get; set; }

        /// <summary>
        /// This is a Equals override method.
        /// </summary>
        /// <param name="obj">Is the basic parameter of this override.</param>
        /// <returns> star identification.</returns>
        public override bool Equals(object obj)
        {
            return (obj as Star).StarID == this.StarID;
        }

        /// <summary>
        /// This is a GetHashCode override method.
        /// </summary>
        /// <returns>a hashcode.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}