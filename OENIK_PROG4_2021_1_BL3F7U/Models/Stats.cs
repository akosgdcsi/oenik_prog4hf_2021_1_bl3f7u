﻿// <copyright file="Stats.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Models
{
    using global::System.Collections.Generic;

    /// <summary>
    /// This is a documation for Stats file.
    /// </summary>
    public class Stats
    {
        /// <summary>
        /// Gets or sets the Stars with habitable planets.
        /// </summary>
        public IEnumerable<Star> StarsWithLife { get; set; }

        /// <summary>
        /// Gets or sets the population number by sectors.
        /// </summary>
        public IEnumerable<PopulationInSec> PopulationInSectors { get; set; }

        /// <summary>
        /// Gets or sets all the planet type where their stars are older then 1 million years.
        /// </summary>
        public IEnumerable<PlanetTypeGrouped> PlanetTypeGrouped { get; set; }
    }
}