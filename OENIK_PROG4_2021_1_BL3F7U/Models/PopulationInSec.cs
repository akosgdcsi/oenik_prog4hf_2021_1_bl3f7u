﻿// <copyright file="PopulationInSec.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Models
{
    /// <summary>
    /// This is a documation for PopulationInSec file.
    /// </summary>
    public class PopulationInSec
    {
        /// <summary>
        /// Gets or sets a sector type.
        /// </summary>
        public string SectorType { get; set; }

        /// <summary>
        /// Gets or sets the population.
        /// </summary>
        public int Population { get; set; }
    }
}