﻿// <copyright file="Planet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Models
{
    using global::System.ComponentModel.DataAnnotations;
    using global::System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Types of Planets.
    /// </summary>
    public enum PlanetType
    {
        /// <summary>
        /// Represents a Miniterran.
        /// </summary>
        Miniterran,

        /// <summary>
        /// Represents a Subterran.
        /// </summary>
        Subterran,

        /// <summary>
        /// Represents a Terran.
        /// </summary>
        Terran,

        /// <summary>
        /// Represents a Superterran.
        /// </summary>
        Superterran,

        /// <summary>
        /// Represents a Neptunian.
        /// </summary>
        Neptunian,

        /// <summary>
        /// Represents a Jovian.
        /// </summary>
        Jovian,
    }

    /// <summary>
    /// This is a documation for Planet file.
    /// </summary>
    public class Planet
    {
        /// <summary>
        /// Gets or sets a planet identification.
        /// </summary>
        [Key]
        public string PlanetID { get; set; }

        /// <summary>
        /// Gets or sets the name of a planet.
        /// </summary>
        [StringLength(200)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of a planet.
        /// </summary>
        public PlanetType PlanetType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is enabled.
        /// </summary>
        public bool Habitable { get; set; }

        /// <summary>
        /// Gets or sets the population of a planet.
        /// </summary>
        public int Population { get; set; }

        /// <summary>
        /// Gets or sets the star identification of a planet.
        /// </summary>
        public string StarID { get; set; }

        /// <summary>
        /// Gets or sets a virtual notmapped .
        /// </summary>
        [NotMapped]
        public virtual Star Star { get; set; }

        /// <summary>
        /// This is a Equals override method.
        /// </summary>
        /// <param name="obj">Is the basic parameter of this override.</param>
        /// <returns> planet identification.</returns>
        public override bool Equals(object obj)
        {
            return (obj as Planet).PlanetID == this.PlanetID;
        }

        /// <summary>
        /// This is a GetHashCode override method.
        /// </summary>
        /// <returns>a hashcode.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}