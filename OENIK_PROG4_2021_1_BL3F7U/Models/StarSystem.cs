﻿// <copyright file="StarSystem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Models
{
    using global::System.Collections.Generic;
    using global::System.ComponentModel.DataAnnotations;

    /// <summary>
    /// This is a documation for System file.
    /// </summary>
    public class StarSystem
    {
        /// <summary>
        /// Gets or sets a system identification.
        /// </summary>
        [Key]
        public string SystemID { get; set; }

        /// <summary>
        /// Gets or sets a system name.
        /// </summary>
        [StringLength(200)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a system's sector name.
        /// </summary>
        [StringLength(200)]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets a virtual icollection .
        /// </summary>
        public virtual ICollection<Star> Stars { get; set; }

        /// <summary>
        /// This is a Equals override method.
        /// </summary>
        /// <param name="obj">Is the basic parameter of this override.</param>
        /// <returns> system identification.</returns>
        public override bool Equals(object obj)
        {
            return (obj as StarSystem).SystemID == this.SystemID;
        }

        /// <summary>
        /// This is a GetHashCode override method.
        /// </summary>
        /// <returns>a hashcode.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}