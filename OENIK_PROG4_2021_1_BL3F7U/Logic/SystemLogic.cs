﻿// <copyright file="SystemLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Linq;
    using Models;
    using Repository;

    /// <summary>
    /// This is a documation for SystemLogic file.
    /// </summary>
    public class SystemLogic
    {
        private IRepository<Planet> planetRepo;
        private IRepository<Star> starRepo;
        private IRepository<Models.StarSystem> systemRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemLogic"/> class.
        /// </summary>
        /// <param name="planetRepo">Is the planetRepo parameter of this constructor.</param>
        /// <param name="starRepo">Is the starRepo parameter of this constructor.</param>
        /// <param name="systemRepo">Is the basic systemRepo of this constructor.</param>
        public SystemLogic(IRepository<Planet> planetRepo, IRepository<Star> starRepo, IRepository<Models.StarSystem> systemRepo)
        {
            this.planetRepo = planetRepo;
            this.starRepo = starRepo;
            this.systemRepo = systemRepo;
        }

        // crud methods

        /// <summary>
        /// This is responsible for adding System.
        /// </summary>
        /// /// <param name="item">Is the Planet object reference.</param>
        public void AddSystem(Models.StarSystem item)
        {
            this.systemRepo.Add(item);
        }

        /// <summary>
        /// This is responsible for deleting systems.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        public void DeleteSystem(string id)
        {
            this.systemRepo.Delete(id);
        }

        /// <summary>
        /// This is responsible for listing all systems.
        /// </summary>
        /// /// <returns>With all systems.</returns>
        public IQueryable<Models.StarSystem> GetAllSystem()
        {
            return this.systemRepo.Read();
        }

        /// <summary>
        /// This is responsible for getting a systems object by its id.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        /// /// <returns>a system object.</returns>
        public Models.StarSystem GetSystem(string id)
        {
            return this.systemRepo.Read(id);
        }

        /// <summary>
        /// This is responsible for updateing systems.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to getting an object.</param>
        /// /// <param name="newitem">Is the System object reference.</param>
        public void UpdateSystem(string id, Models.StarSystem newitem)
        {
            this.systemRepo.Update(id, newitem);
        }

        /// <summary>
        /// This is responsible for getting a planet that belongs to id system.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to getting an object.</param>
        /// /// <returns>all the stars that belongs to a system.</returns>
        public IQueryable<Star> GetStar(string id)
        {
            return this.systemRepo.Read(id).Stars.AsQueryable();
        }
    }
}