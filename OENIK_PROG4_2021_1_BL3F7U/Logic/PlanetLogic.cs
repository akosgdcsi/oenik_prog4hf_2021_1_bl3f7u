﻿// <copyright file="PlanetLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace Logic
{
    using System.Linq;
    using GalaSoft.MvvmLight.Ioc;
    using Models;
    using Repository;

    /// <summary>
    /// This is a documation for PlanetLogic file.
    /// </summary>
    public class PlanetLogic
    {
        private IRepository<Planet> planetRepo;
        private IRepository<Star> starRepo;
        private IRepository<Models.StarSystem> systemRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlanetLogic"/> class.
        /// </summary>
        /// <param name="planetRepo">Is the planetRepo parameter of this constructor.</param>
        /// <param name="starRepo">Is the starRepo parameter of this constructor.</param>
        /// <param name="systemRepo">Is the basic systemRepo of this constructor.</param>
        public PlanetLogic(IRepository<Planet> planetRepo, IRepository<Star> starRepo, IRepository<Models.StarSystem> systemRepo)
        {
            this.planetRepo = planetRepo;
            this.starRepo = starRepo;
            this.systemRepo = systemRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlanetLogic"/> class.
        /// </summary>
        /// <param name="planetRepo">Is the planetRepo parameter of this constructor.</param>
        [PreferredConstructorAttribute]
        public PlanetLogic(IRepository<Planet> planetRepo)
        {
            this.planetRepo = planetRepo;
        }

        // crud methods

        /// <summary>
        /// This is responsible for adding planets.
        /// </summary>
        /// /// <param name="item">Is the Planet object reference.</param>
        public void AddPlanet(Planet item)
        {
            this.planetRepo.Add(item);
        }

        /// <summary>
        /// This is responsible for deleting planets.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        public void DeletePlanet(string id)
        {
            this.planetRepo.Delete(id);
        }

        /// <summary>
        /// This is responsible for listing all planets.
        /// </summary>
        /// /// <returns>With all planets.</returns>
        public IQueryable<Planet> GetAllPlanet()
        {
            return this.planetRepo.Read();
        }

        /// <summary>
        /// This is responsible for getting a planet object by its id.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        /// /// <returns>a planet object.</returns>
        public Planet GetPlanet(string id)
        {
            return this.planetRepo.Read(id);
        }

        /// <summary>
        /// This is responsible for updateing planets.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to getting an object.</param>
        /// /// <param name="newitem">Is the Planet object reference.</param>
        public void UpdatePlanet(string id, Planet newitem)
        {
            this.planetRepo.Update(id, newitem);
        }

        /// <summary>
        /// This is responsible for getting all of the planets that belongs to id star.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to getting an object.</param>
        /// /// <returns>all the stars to a specific system.</returns>
        public IQueryable<Planet> PlanetToStar(string id)
        {
            var q = this.planetRepo.Read().Select(x => x).Where(x => x.StarID == id);

            return q.AsQueryable();
        }
    }
}