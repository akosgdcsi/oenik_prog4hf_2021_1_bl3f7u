﻿// <copyright file="StarLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Linq;
    using Models;
    using Repository;

    /// <summary>
    /// This is a documation for StarLogic file.
    /// </summary>
    public class StarLogic
    {
        private IRepository<Planet> planetRepo;
        private IRepository<Star> starRepo;
        private IRepository<Models.StarSystem> systemRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="StarLogic"/> class.
        /// </summary>
        /// <param name="planetRepo">Is the planetRepo parameter of this constructor.</param>
        /// <param name="starRepo">Is the starRepo parameter of this constructor.</param>
        /// <param name="systemRepo">Is the basic systemRepo of this constructor.</param>
        public StarLogic(IRepository<Planet> planetRepo, IRepository<Star> starRepo, IRepository<Models.StarSystem> systemRepo)
        {
            this.planetRepo = planetRepo;
            this.starRepo = starRepo;
            this.systemRepo = systemRepo;
        }

        // crud methods

        /// <summary>
        /// This is responsible for adding stars.
        /// </summary>
        /// /// <param name="item">Is the Planet object reference.</param>
        public void AddStar(Star item)
        {
            this.starRepo.Add(item);
        }

        /// <summary>
        /// This is responsible for deleting stars.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        public void DeleteStar(string id)
        {
            this.starRepo.Delete(id);
        }

        /// <summary>
        /// This is responsible for listing all stars.
        /// </summary>
        /// /// <returns>With all stars.</returns>
        public IQueryable<Star> GetAllStar()
        {
            return this.starRepo.Read();
        }

        /// <summary>
        /// This is responsible for getting a stars object by its id.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        /// /// <returns>a star object.</returns>
        public Star GetStar(string id)
        {
            return this.starRepo.Read(id);
        }

        /// <summary>
        /// This is responsible for updateing stars.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to getting an object.</param>
        /// /// <param name="newitem">Is the Star object reference.</param>
        public void UpdateStar(string id, Star newitem)
        {
            this.starRepo.Update(id, newitem);
        }

        /// <summary>
        /// This is responsible for getting all of the planets that belongs to id star.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to getting an object.</param>
        /// /// <returns>all the stars to a specific system.</returns>
        public IQueryable<Star> StarToSystem(string id)
        {
            var q = this.starRepo.Read().Select(x => x).Where(x => x.SystemID == id);

            return q.AsQueryable();
        }

        /// <summary>
        /// This is responsible for getting a planet that belongs to id star.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to getting an object.</param>
        /// /// <returns>all the planets that belongs to a star.</returns>
        public IQueryable<Planet> GetPlanet(string id)
        {
            return this.starRepo.Read(id).Planets.AsQueryable();
        }
    }
}