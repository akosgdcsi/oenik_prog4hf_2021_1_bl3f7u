﻿// <copyright file="StatsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using Models;
    using Repository;

    /// <summary>
    /// This is a documation for StatsLogic file.
    /// </summary>
    public class StatsLogic
    {
        private IRepository<Planet> planetRepo;
        private IRepository<Star> starRepo;
        private IRepository<Models.StarSystem> systemRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatsLogic"/> class.
        /// </summary>
        /// <param name="planetRepo">Is the planetRepo parameter of this constructor.</param>
        /// <param name="starRepo">Is the starRepo parameter of this constructor.</param>
        /// <param name="systemRepo">Is the basic systemRepo of this constructor.</param>
        public StatsLogic(IRepository<Planet> planetRepo, IRepository<Star> starRepo, IRepository<Models.StarSystem> systemRepo)
        {
            this.planetRepo = planetRepo;
            this.starRepo = starRepo;
            this.systemRepo = systemRepo;
        }

        /// <summary>
        /// This is responsible for finding out which satars have habitable planets.
        /// </summary>
        /// <returns>With specific stars.</returns>
        public IEnumerable<Star> StarsWithLife()
        {
            var q1 = from star in this.starRepo.Read().ToList()
                     join planet in this.planetRepo.Read().ToList() on star.StarID equals planet.StarID
                     where planet.Habitable
                     select star;
            return q1;
        }

        /// <summary>
        /// This is responsible for finding out the population by sectors.
        /// </summary>
        /// <returns>With PopulationInSec object.</returns>
        public IEnumerable<PopulationInSec> PopulationInSectors()
        {
            var q2 = (from system in this.systemRepo.Read().ToList()
                      join star in this.starRepo.Read().ToList() on system.SystemID equals star.SystemID
                      join planet in this.planetRepo.Read().ToList() on star.StarID equals planet.StarID
                      group system by system.SectorName into g
                      select new PopulationInSec
                      {
                          SectorType = g.Key,
                          Population = g.SelectMany(sys => sys.Stars).SelectMany(s => s.Planets).Distinct().Sum(p => p.Population),
                      }).OrderByDescending(x => x.Population);
            return q2.AsQueryable();
        }

        /// <summary>
        /// This is responsible for getting all the planet type where their stars are older then 1 million years.
        /// </summary>
        /// <returns>With PlanetTypeGrouped object.</returns>
        public IEnumerable<PlanetTypeGrouped> PlanetTypeGrouped()
        {
            var q3 = from planet in this.planetRepo.Read().ToList()
                     join star in this.starRepo.Read().ToList() on planet.StarID equals star.StarID
                     where star.Age > 1000000
                     group planet by planet.PlanetType into g
                     select new PlanetTypeGrouped
                     {
                         Type = g.Key,
                         NumberOfStars = g.Select(x => x.StarID).Distinct().Count(),
                     };
            return q3.AsQueryable();
        }
    }
}