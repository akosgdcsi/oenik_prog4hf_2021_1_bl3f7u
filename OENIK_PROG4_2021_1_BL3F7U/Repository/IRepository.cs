﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace Repository
{
    using System.Linq;

    /// <summary>
    /// This is a documation for IRepository file.
    /// </summary>
    /// /// <typeparam name="T">The first generic type parameter.</typeparam>
    public interface IRepository<T>
        where T : new()
    {
        /// <summary>
        /// This is responsible for adding T object.
        /// </summary>
        /// /// <param name="item">Is the T object reference.</param>
        void Add(T item);

        /// <summary>
        /// This is responsible for deleting T object.
        /// </summary>
        /// /// <param name="item">Is the T object reference.</param>
        void Delete(T item);

        /// <summary>
        /// This is responsible for deleting T object.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        void Delete(string id);

        /// <summary>
        /// This is responsible getting the system object by it's identification.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        /// /// <returns>The T object.</returns>
        T Read(string id);

        /// <summary>
        /// This is responsible getting all of the T object.
        /// </summary>
        /// <returns>a iQuaryable T objects.</returns>
        IQueryable<T> Read();

        /// <summary>
        /// This is responsible updating the T objects.
        /// </summary>
        /// <param name="oldid">Is the identification that we need to update an object.</param>
        /// <param name="newitem">Is the updated T item.</param>
        void Update(string oldid, T newitem);

        /// <summary>
        /// This is responsible saving the current datacontext.
        /// </summary>
        void Save();
    }
}