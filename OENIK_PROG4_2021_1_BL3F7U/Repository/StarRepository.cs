﻿// <copyright file="StarRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System.Linq;
    using Data;
    using Models;

    /// <summary>
    /// This is a documation for StarRepository file.
    /// </summary>
    public class StarRepository : IRepository<Star>
    {
        private SolarContext context = new SolarContext();

        /// <summary>
        /// This is responsible for adding stars.
        /// </summary>
        /// /// <param name="item">Is the Star object reference.</param>
        public void Add(Star item)
        {
            this.context.Stars.Add(item);
            this.context.SaveChanges();
        }

        /// <summary>
        /// This is responsible for deleting stars.
        /// </summary>
        /// /// <param name="item">Is the Star object reference.</param>
        public void Delete(Star item)
        {
            this.context.Stars.Remove(item);
            this.context.SaveChanges();
        }

        /// <summary>
        /// This is responsible for deleting stars.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        public void Delete(string id)
        {
            this.Delete(this.Read(id));
        }

        /// <summary>
        /// This is responsible getting the star object by it's identification.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        /// <returns>a star object.</returns>
        public Star Read(string id)
        {
            return this.context.Stars.FirstOrDefault(t => t.StarID == id);
        }

        /// <summary>
        /// This is responsible getting all of the stars.
        /// </summary>
        /// <returns>a iQuaryable star.</returns>
        public IQueryable<Star> Read()
        {
            return this.context.Stars.AsQueryable();
        }

        /// <summary>
        /// This is responsible saving the current datacontext.
        /// </summary>
        public void Save()
        {
            this.context.SaveChanges();
        }

        /// <summary>
        /// This is responsible updateing of the stars.
        /// </summary>
        /// <param name="oldid">Is the identification that we need to update an object.</param>
        /// <param name="newitem">Is the updated Star item.</param>
        public void Update(string oldid, Star newitem)
        {
            var olditem = this.Read(oldid);
            olditem.Name = newitem.Name;
            olditem.StarType = newitem.StarType;
            olditem.Age = newitem.Age;
            olditem.Planets = newitem.Planets;

            this.context.SaveChanges();
        }
    }
}