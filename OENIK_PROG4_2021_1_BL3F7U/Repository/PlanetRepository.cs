﻿// <copyright file="PlanetRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System.Linq;
    using Data;
    using Models;

    /// <summary>
    /// This is a documation for PlanetRepository file.
    /// </summary>
    public class PlanetRepository : IRepository<Planet>
    {
        private SolarContext context = new SolarContext();

        /// <summary>
        /// This is responsible for adding planets.
        /// </summary>
        /// /// <param name="item">Is the Planet object reference.</param>
        public void Add(Planet item)
        {
            this.context.Planets.Add(item);
            this.context.SaveChanges();
        }

        /// <summary>
        /// This is responsible for deleting planets.
        /// </summary>
        /// /// <param name="item">Is the Star object reference.</param>
        public void Delete(Planet item)
        {
            this.context.Planets.Remove(item);
            this.context.SaveChanges();
        }

        /// <summary>
        /// This is responsible for deleting planets.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        public void Delete(string id)
        {
            this.Delete(this.Read(id));
        }

        /// <summary>
        /// This is responsible getting the planet object by it's identification.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        /// <returns>a planet object.</returns>
        public Planet Read(string id)
        {
            return this.context.Planets.FirstOrDefault(t => t.PlanetID == id);
        }

        /// <summary>
        /// This is responsible getting all of the planets.
        /// </summary>
        /// <returns>a iQuaryable planet.</returns>
        public IQueryable<Planet> Read()
        {
            return this.context.Planets.AsQueryable();
        }

        /// <summary>
        /// This is responsible saving the current datacontext.
        /// </summary>
        public void Save()
        {
            this.context.SaveChanges();
        }

        /// <summary>
        /// This is responsible updating the planets.
        /// </summary>
        /// <param name="oldid">Is the identification that we need to update an object.</param>
        /// <param name="newitem">Is the updated Planet item.</param>
        public void Update(string oldid, Planet newitem)
        {
            var olditem = this.Read(oldid);
            olditem.Name = newitem.Name;
            olditem.PlanetType = newitem.PlanetType;
            olditem.Habitable = newitem.Habitable;
            olditem.Population = newitem.Population;

            this.context.SaveChanges();
        }
    }
}