﻿// <copyright file="SystemRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System.Linq;
    using Data;

    /// <summary>
    /// This is a documation for SystemRepository file.
    /// </summary>
    public class SystemRepository : IRepository<Models.StarSystem>
    {
        private SolarContext context = new SolarContext();

        /// <summary>
        /// This is responsible for adding systems.
        /// </summary>
        /// /// <param name="item">Is the Planet object reference.</param>
        public void Add(Models.StarSystem item)
        {
            this.context.Systems.Add(item);
            this.context.SaveChanges();
        }

        /// <summary>
        /// This is responsible for deleting stars.
        /// </summary>
        /// /// <param name="item">Is the Star object reference.</param>
        public void Delete(Models.StarSystem item)
        {
            this.context.Systems.Remove(item);
            this.context.SaveChanges();
        }

        /// <summary>
        /// This is responsible for deleting systems.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        public void Delete(string id)
        {
            this.Delete(this.Read(id));
        }

        /// <summary>
        /// This is responsible getting the system object by it's identification.
        /// </summary>
        /// /// <param name="id">Is the identification that we need to delete an object.</param>
        /// <returns>a planet object.</returns>
        public Models.StarSystem Read(string id)
        {
            return this.context.Systems.FirstOrDefault(t => t.SystemID == id);
        }

        /// <summary>
        /// This is responsible getting all of the systems.
        /// </summary>
        /// <returns>a iQuaryable planet.</returns>
        public IQueryable<Models.StarSystem> Read()
        {
            return this.context.Systems.AsQueryable();
        }

        /// <summary>
        /// This is responsible saving the current datacontext.
        /// </summary>
        public void Save()
        {
            this.context.SaveChanges();
        }

        /// <summary>
        /// This is responsible updating the systems.
        /// </summary>
        /// <param name="oldid">Is the identification that we need to update an object.</param>
        /// <param name="newitem">Is the updated Systems item.</param>
        public void Update(string oldid, Models.StarSystem newitem)
        {
            var olditem = this.Read(oldid);
            olditem.Name = newitem.Name;
            olditem.SectorName = newitem.SectorName;
            olditem.Stars = newitem.Stars;

            this.context.SaveChanges();
        }
    }
}