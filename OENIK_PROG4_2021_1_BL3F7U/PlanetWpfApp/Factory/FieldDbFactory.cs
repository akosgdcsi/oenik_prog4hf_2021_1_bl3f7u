﻿// <copyright file="FieldDbFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlanetWpfApp.Factory
{
    using System;
    using Logic;

    /// <summary>
    /// This is a documation for FieldDbFactory file.
    /// </summary>
    internal class FieldDbFactory
    {
        private Logic.PlanetLogic planetLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldDbFactory"/> class.
        /// </summary>
        /// <param name="planetLogic">Is the planetRepo parameter of this constructor.</param>
        public FieldDbFactory(PlanetLogic planetLogic)
        {
            this.planetLogic = planetLogic;
        }

        /// <summary>
        /// This is responsible for generateing data.
        /// </summary>
        public void GenerateData()
        {
            Models.Planet p1 = new Models.Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Kepler-6543B2", Habitable = true, PlanetType = Models.PlanetType.Terran, Population = 10, };
            this.planetLogic.AddPlanet(p1);
            Models.Planet p2 = new Models.Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Branco", Habitable = false, PlanetType = Models.PlanetType.Superterran, Population = 0, };
            this.planetLogic.AddPlanet(p2);
            Models.Planet p3 = new Models.Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Elizabeth", Habitable = false, PlanetType = Models.PlanetType.Jovian, Population = 0, };
            this.planetLogic.AddPlanet(p3);
            Models.Planet p4 = new Models.Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Gemini-65422A", Habitable = false, PlanetType = Models.PlanetType.Superterran, Population = 0, };
            this.planetLogic.AddPlanet(p4);
            Models.Planet p5 = new Models.Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Pisces-493294", Habitable = false, PlanetType = Models.PlanetType.Jovian, Population = 0, };
            this.planetLogic.AddPlanet(p5);
        }
    }
}