﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlanetWpfApp.UI
{
    using PlanetWpfApp.BL;
    using PlanetWpfApp.Data;

    /// <summary>
    /// This is a documation for EditorServiceViaWindow file.
    /// </summary>
    internal class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// This is responsible for editing a planet object.
        /// </summary>
        /// /// <param name="p">Is the Planet object reference.</param>
        /// <returns>an edited planet.</returns>
        public bool EditPlayer(Planet p)
        {
            EditorWindow win = new EditorWindow(p);
            return win.ShowDialog() ?? false;
        }
    }
}