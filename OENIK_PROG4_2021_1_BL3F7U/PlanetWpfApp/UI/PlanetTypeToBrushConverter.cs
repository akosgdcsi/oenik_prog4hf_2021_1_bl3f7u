﻿// <copyright file="PlanetTypeToBrushConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlanetWpfApp.UI
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media;
    using PlanetWpfApp.Data;

    /// <summary>
    /// This is a documation for PlanetTypeToBrushConverter file.
    /// </summary>
    internal class PlanetTypeToBrushConverter : IValueConverter
    {
        /// <summary>
        /// This is responsible for converting an object.
        /// </summary>
        /// /// <param name="value">Is the object value reference.</param>
        /// /// <param name="targetType">Is the type reference.</param>
        /// /// <param name="parameter">Is the object parameter reference.</param>
        /// /// <param name="culture">Is the CultureInfo reference.</param>
        /// <returns>with a brush color.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            PlanetType type = (PlanetType)value;
            switch (type)
            {
                default:
                case PlanetType.Jovian:
                    return Brushes.SaddleBrown;

                case PlanetType.Miniterran:
                    return Brushes.MediumVioletRed;

                case PlanetType.Neptunian:
                    return Brushes.AliceBlue;

                case PlanetType.Subterran:
                    return Brushes.Salmon;

                case PlanetType.Superterran:
                    return Brushes.SandyBrown;

                case PlanetType.Terran:
                    return Brushes.BlueViolet;
            }
        }

        /// <summary>
        /// This is responsible for converting back an object.
        /// </summary>
        /// /// <param name="value">Is the object value reference.</param>
        /// /// <param name="targetType">Is the type reference.</param>
        /// /// <param name="parameter">Is the object parameter reference.</param>
        /// /// <param name="culture">Is the CultureInfo reference.</param>
        /// <returns>with a donothing.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}