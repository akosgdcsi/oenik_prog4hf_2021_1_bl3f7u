﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlanetWpfApp
{
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using PlanetWpfApp.BL;
    using PlanetWpfApp.Factory;
    using PlanetWpfApp.UI;
    using Repository;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        private FieldDbFactory fieldDb = new FieldDbFactory(new Logic.PlanetLogic(new PlanetRepository()));

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);

            MyIoc.Instance.Register<IEditorService, EditorServiceViaWindow>();
            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIoc.Instance.Register<IPlanetLogic, PlanetLogic>();
            MyIoc.Instance.Register<Logic.PlanetLogic>();
            MyIoc.Instance.Register<IRepository<Models.Planet>, PlanetRepository>();
            this.fieldDb.GenerateData();
        }
    }
}