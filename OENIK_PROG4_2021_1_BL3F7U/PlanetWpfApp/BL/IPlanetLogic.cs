﻿// <copyright file="IPlanetLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlanetWpfApp.BL
{
    using System.Collections.Generic;
    using PlanetWpfApp.Data;

    /// <summary>
    /// This is a documation for IPlanetLogic file.
    /// </summary>
    internal interface IPlanetLogic
    {
        /// <summary>
        /// This is responsible for adding a planet object.
        /// </summary>
        /// /// <param name="list">Is the list reference.</param>
        void AddPlanet(IList<Planet> list);

        /// <summary>
        /// This is responsible for modifying a planet object.
        /// </summary>
        /// /// <param name="planetToModify">Is the T object reference.</param>
        void ModPlanet(Planet planetToModify);

        /// <summary>
        /// This is responsible for editing a planet object.
        /// </summary>
        /// <param name="list">Is IList Planet reference.</param>
        /// <param name="planet">Is the Planet object reference.</param>
        void DelPlanet(IList<Planet> list, Planet planet);

        /// <summary>
        /// This is responsible for editing a planet object.
        /// </summary>
        /// <returns>with every planet in the database.</returns>
        IList<Planet> GetALL();
    }
}