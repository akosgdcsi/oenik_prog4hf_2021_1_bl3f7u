﻿// <copyright file="PlanetLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlanetWpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using PlanetWpfApp.Data;

    /// <summary>
    /// This is a documation for PlanetLogic file.
    /// </summary>
    internal class PlanetLogic : IPlanetLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private Logic.PlanetLogic planet1;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlanetLogic"/> class.
        /// </summary>
        /// <param name="editorService">Is the planetRepo parameter of this constructor.</param>
        /// <param name="messengerService">Is the starRepo parameter of this constructor.</param>
        /// <param name="planet1">Is the planet1 parameter of this constructor.</param>
        public PlanetLogic(IEditorService editorService, IMessenger messengerService, Logic.PlanetLogic planet1)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.planet1 = planet1;
        }

        /// <summary>
        /// This is responsible for adding a planet object.
        /// </summary>
        /// /// <param name="list">Is the list reference.</param>
        public void AddPlanet(IList<Planet> list)
        {
            Planet newPlanet = new Planet();
            if (this.editorService.EditPlayer(newPlanet) == true)
            {
                Models.Planet newerPlanet = new Models.Planet() { PlanetID = Guid.NewGuid().ToString(), Name = newPlanet.Name, Habitable = newPlanet.Habitable };
                newPlanet.PlanetID = newerPlanet.PlanetID;
                switch (newPlanet.PlanetType)
                {
                    default:
                    case PlanetType.Jovian:
                        newerPlanet.PlanetType = Models.PlanetType.Jovian;
                        break;

                    case PlanetType.Miniterran:
                        newerPlanet.PlanetType = Models.PlanetType.Miniterran;
                        break;

                    case PlanetType.Neptunian:
                        newerPlanet.PlanetType = Models.PlanetType.Neptunian;
                        break;

                    case PlanetType.Subterran:
                        newerPlanet.PlanetType = Models.PlanetType.Subterran;
                        break;

                    case PlanetType.Superterran:
                        newerPlanet.PlanetType = Models.PlanetType.Superterran;
                        break;

                    case PlanetType.Terran:
                        newerPlanet.PlanetType = Models.PlanetType.Terran;
                        break;
                }

                list.Add(newPlanet);

                // Call DB operation...
                this.planet1.AddPlanet(newerPlanet);
                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// This is responsible for editing a planet object.
        /// </summary>
        /// <param name="list">Is IList Planet reference.</param>
        /// <param name="planet">Is the Planet object reference.</param>
        public void DelPlanet(IList<Planet> list, Planet planet)
        {
            if (planet != null && list.Remove(planet))
            {
                this.planet1.DeletePlanet(planet.PlanetID);
                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <summary>
        /// This is responsible for editing a planet object.
        /// </summary>
        /// <returns>with every planet in the database.</returns>
        public IList<Planet> GetALL()
        {
            List<Models.Planet> planets = this.planet1.GetAllPlanet().ToList();
            List<Planet> frontPlanet = new List<Planet>();

            foreach (var item in planets)
            {
                Planet p = new Planet()
                {
                    PlanetID = item.PlanetID,
                    Name = item.Name,
                    Pobulation = item.Population,
                    Habitable = item.Habitable,
                };

                switch (item.PlanetType)
                {
                    default:
                    case Models.PlanetType.Jovian:
                        p.PlanetType = PlanetType.Jovian;
                        break;

                    case Models.PlanetType.Miniterran:
                        p.PlanetType = PlanetType.Miniterran;
                        break;

                    case Models.PlanetType.Neptunian:
                        p.PlanetType = PlanetType.Neptunian;
                        break;

                    case Models.PlanetType.Subterran:
                        p.PlanetType = PlanetType.Subterran;
                        break;

                    case Models.PlanetType.Superterran:
                        p.PlanetType = PlanetType.Superterran;
                        break;

                    case Models.PlanetType.Terran:
                        p.PlanetType = PlanetType.Terran;
                        break;
                }

                frontPlanet.Add(p);
            }

            return frontPlanet;
        }

        /// <summary>
        /// This is responsible for modifying a planet object.
        /// </summary>
        /// /// <param name="planetToModify">Is the T object reference.</param>
        public void ModPlanet(Planet planetToModify)
        {
            if (planetToModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            Planet clone = new Planet();
            clone.CopyFrom(planetToModify);
            if (this.editorService.EditPlayer(clone) == true)
            {
                planetToModify.CopyFrom(clone);

                // Call DB operation...
                Models.Planet newerPlanet = new Models.Planet() { PlanetID = clone.PlanetID, Name = clone.Name, Habitable = clone.Habitable };

                switch (clone.PlanetType)
                {
                    default:
                    case PlanetType.Jovian:
                        newerPlanet.PlanetType = Models.PlanetType.Jovian;
                        break;

                    case PlanetType.Miniterran:
                        newerPlanet.PlanetType = Models.PlanetType.Miniterran;
                        break;

                    case PlanetType.Neptunian:
                        newerPlanet.PlanetType = Models.PlanetType.Neptunian;
                        break;

                    case PlanetType.Subterran:
                        newerPlanet.PlanetType = Models.PlanetType.Subterran;
                        break;

                    case PlanetType.Superterran:
                        newerPlanet.PlanetType = Models.PlanetType.Superterran;
                        break;

                    case PlanetType.Terran:
                        newerPlanet.PlanetType = Models.PlanetType.Terran;
                        break;
                }

                this.planet1.UpdatePlanet(clone.PlanetID, newerPlanet);
                this.messengerService.Send("MODIFY OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("MODIFY CANCEL", "LogicResult");
            }
        }
    }
}