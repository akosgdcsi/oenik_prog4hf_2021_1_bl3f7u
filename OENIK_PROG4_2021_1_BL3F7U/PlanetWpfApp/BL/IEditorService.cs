﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlanetWpfApp.BL
{
    using PlanetWpfApp.Data;

    /// <summary>
    /// This is a documation for PlanetLogic file.
    /// </summary>
    internal interface IEditorService
    {
        /// <summary>
        /// This is responsible for editing a planet object.
        /// </summary>
        /// /// <param name="p">Is the Planet object reference.</param>
        /// <returns>an edited planet.</returns>
        bool EditPlayer(Planet p);
    }
}