﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlanetWpfApp.VM
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using PlanetWpfApp.BL;
    using PlanetWpfApp.Data;

    /// <summary>
    /// This is a documation for MainViewModel file.
    /// </summary>
    internal class MainViewModel : ViewModelBase
    {
        private IPlanetLogic logic;
        private Planet starSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">Is the planetRepo parameter of this constructor.</param>
        public MainViewModel(IPlanetLogic logic)
        {
            this.logic = logic;
            if (!this.IsInDesignMode)
            {
                this.Star = new ObservableCollection<Planet>(this.logic.GetALL());
            }

            if (this.IsInDesignMode)
            {
                Planet p2 = new Planet() { Name = "Wild Bill 2" };
                Planet p3 = new Planet() { Name = "Wild Bill 3", PlanetType = PlanetType.Terran };
                this.Star.Add(p2);
                this.Star.Add(p3);
            }

            this.AddCmd = new RelayCommand(() => this.logic.AddPlanet(this.Star));
            this.ModCmd = new RelayCommand(() => this.logic.ModPlanet(this.StarSelected));
            this.DelCmd = new RelayCommand(() => this.logic.DelPlanet(this.Star, this.StarSelected));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IPlanetLogic>())
        {
        }

        /// <summary>
        /// Gets a collection of planets.
        /// </summary>
        public ObservableCollection<Planet> Star { get; private set; }

        /// <summary>
        /// Gets or sets the selected planet type.
        /// </summary>
        public Planet StarSelected
        {
            get { return this.starSelected; }
            set { this.Set(ref this.starSelected, value); }
        }

        /// <summary>
        /// Gets an add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets an modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets an delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }
    }
}