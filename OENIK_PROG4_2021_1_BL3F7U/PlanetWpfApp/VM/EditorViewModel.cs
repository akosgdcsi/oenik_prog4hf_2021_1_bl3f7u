﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlanetWpfApp.VM
{
    using System;
    using GalaSoft.MvvmLight;
    using PlanetWpfApp.Data;

    /// <summary>
    /// This is a documation for EditorViewModel file.
    /// </summary>
    public partial class EditorViewModel : ViewModelBase
    {
        private Planet planet;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.planet = new Planet();
            if (this.IsInDesignMode)
            {
                this.planet.Name = "Unknown Bill";
                this.planet.Habitable = false;
                this.planet.PlanetType = PlanetType.Subterran;
            }
        }

        /// <summary>
        /// Gets or sets a planet object.
        /// </summary>
        public Planet Planet
        {
            get { return this.planet; }
            set { this.Set(ref this.planet, value); }
        }

        /// <summary>
        /// Gets Planet types.
        /// </summary>
        public Array Planets
        {
            get { return Enum.GetValues(typeof(PlanetType)); }
        }
    }
}