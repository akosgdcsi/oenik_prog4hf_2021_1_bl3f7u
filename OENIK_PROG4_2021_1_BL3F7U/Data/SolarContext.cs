﻿// <copyright file="SolarContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace Data
{
    using Microsoft.EntityFrameworkCore;
    using Models;

    /// <summary>
    /// This is a documation for Solarcontext file.
    /// </summary>
    public class SolarContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SolarContext"/> class.
        /// </summary>
        /// /// <param name="opt">The is the basic parameter of this constructor.</param>
        public SolarContext(DbContextOptions<SolarContext> opt)
            : base(opt)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SolarContext"/> class.
        /// </summary>
        public SolarContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets the Planets of the SolarContext.
        /// </summary>
        public DbSet<Planet> Planets { get; set; }

        /// <summary>
        /// Gets or sets the Stars of the SolarContext.
        /// </summary>
        public DbSet<Star> Stars { get; set; }

        /// <summary>
        /// Gets or sets the Systems of the SolarContext.
        /// </summary>
        public DbSet<Models.StarSystem> Systems { get; set; }

        /// <summary>
        /// This is a OnConfiguring override method.
        /// </summary>
        /// /// <param name="optionsBuilder">Is the basic parameter of this override.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.
                    UseLazyLoadingProxies().
                    UseSqlServer(@"data source=(LocalDB)\MSSQLLocalDB;attachdbfilename=|DataDirectory|\Solar.mdf;integrated security=True;MultipleActiveResultSets=True");
            }
        }

        /// <summary>
        /// This is a OnModelCreating override method.
        /// </summary>
        /// /// <param name="modelBuilder">Is the basic parameter of this override.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Planet>(entitiy =>
            {
                entitiy.HasOne(planet => planet.Star).WithMany(star => star.Planets).HasForeignKey(planet => planet.StarID).OnDelete(DeleteBehavior.Cascade);
            });
            modelBuilder.Entity<Star>(entitiy =>
            {
                entitiy.HasOne(star => star.System).WithMany(system => system.Stars).HasForeignKey(star => star.SystemID).OnDelete(DeleteBehavior.Cascade);
            });
        }
    }
}