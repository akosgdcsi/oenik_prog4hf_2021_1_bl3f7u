﻿// <copyright file="PlanetListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MVC.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// init a PlanetList ViewModel.
    /// </summary>
    public class PlanetListViewModel
    {
        /// <summary>
        /// Gets or sets a planet list.
        /// </summary>
        public List<Planet> ListOFPlanets { get; set; }

        /// <summary>
        /// Gets or sets an edited planet object.
        /// </summary>
        public Planet EditedPlanet { get; set; }
    }
}