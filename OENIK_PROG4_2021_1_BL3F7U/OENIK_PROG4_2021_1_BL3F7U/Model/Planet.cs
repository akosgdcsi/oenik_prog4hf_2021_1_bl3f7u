﻿// <copyright file="Planet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MVC.Model
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Types of Planets.
    /// </summary>
    public enum PlanetType
    {
        /// <summary>
        /// Represents a Miniterran.
        /// </summary>
        Miniterran,

        /// <summary>
        /// Represents a Subterran.
        /// </summary>
        Subterran,

        /// <summary>
        /// Represents a Terran.
        /// </summary>
        Terran,

        /// <summary>
        /// Represents a Superterran.
        /// </summary>
        Superterran,

        /// <summary>
        /// Represents a Neptunian.
        /// </summary>
        Neptunian,

        /// <summary>
        /// Represents a Jovian.
        /// </summary>
        Jovian,
    }

    /// <summary>
    /// This is a documation for Planet file.
    /// </summary>
    public class Planet
    {
        /// <summary>
        /// Gets or sets a planet identification.
        /// </summary>
        [Display(Name = "Planet ID")]
        [Required]
        public string PlanetID { get; set; }

        /// <summary>
        /// Gets or sets the name of a planet.
        /// </summary>
        [Display(Name = "Planet Name")]
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of a planet.
        /// </summary>
        [Display(Name = "Planet Type")]
        [Required]
        public PlanetType PlanetType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is enabled.
        /// </summary>
        [Display(Name = "Planet Habitable")]
        [Required]
        public bool Habitable { get; set; }

        /// <summary>
        /// Gets or sets the population of a planet.
        /// </summary>
        [Display(Name = "Planet Population")]
        [Required]
        public int Population { get; set; }
    }
}