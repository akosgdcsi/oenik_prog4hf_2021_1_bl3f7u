﻿// <copyright file="ApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MVC.Model
{
    /// <summary>
    /// init an api result.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether an operation result is true or false.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}