﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MVC.Model
{
    using AutoMapper;

    /// <summary>
    /// initialze a mapper factory.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Creats mapper between thw vms.
        /// </summary>
        /// <returns>Automapper interface.</returns>
        public static IMapper CreatMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Models.Planet, MVC.Model.Planet>().ForMember(dest => dest.PlanetID, map => map.MapFrom(src => src.PlanetID)).
                ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                ForMember(dest => dest.PlanetType, map => map.MapFrom(src => src.PlanetType)).
                ForMember(dest => dest.Habitable, map => map.MapFrom(src => src.Habitable)).
                ForMember(dest => dest.Population, map => map.MapFrom(src => src.Population));
            });
            return config.CreateMapper();
        }
    }
}