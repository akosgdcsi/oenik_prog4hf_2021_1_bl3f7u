﻿// <copyright file="HomeController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MVC.Controllers
{
    using System;
    using Logic;
    using Microsoft.AspNetCore.Mvc;
    using Models;

    /// <summary>
    /// This is a documation for HomeController file.
    /// </summary>
    public class HomeController : Controller
    {
        private static bool filled = true;
        private PlanetLogic planetLogic;
        private StarLogic starLogic;
        private SystemLogic systemLogic;
        private StatsLogic statsLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="planetLogic">Is the planetLogic parameter of this constructor.</param>
        /// <param name="starLogic">Is the starLogic parameter of this constructor.</param>
        /// <param name="systemLogic">Is the basic systemLogic of this constructor.</param>
        /// <param name="statsLogic">Is the basic statsLogic of this constructor.</param>
        public HomeController(PlanetLogic planetLogic, StarLogic starLogic, SystemLogic systemLogic, StatsLogic statsLogic)
        {
            this.planetLogic = planetLogic;
            this.starLogic = starLogic;
            this.systemLogic = systemLogic;
            this.statsLogic = statsLogic;
        }

        /// <summary>
        /// Creating a View for index page.
        /// </summary>
        /// /// <returns>With IActionResult.</returns>
        public IActionResult Index()
        {
            return this.View();
        }

        // System

        /// <summary>
        /// Creating a View for adding systems.
        /// </summary>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult AddSystem()
        {
            return this.View();
        }

        /// <summary>
        /// Adds system to database.
        /// </summary>
        /// <param name="s">Is the System object reference.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpPost]
        public IActionResult AddSystem(Models.StarSystem s)
        {
            s.SystemID = Guid.NewGuid().ToString();

            this.systemLogic.AddSystem(s);
            return this.RedirectToAction(nameof(this.ListSystem));
        }

        /// <summary>
        /// Creating a View for listing systems.
        /// </summary>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult ListSystem()
        {
            return this.View(this.systemLogic.GetAllSystem());
        }

        /// <summary>
        /// Creating a View for listing stars to systems.
        /// </summary>
        /// <param name="id">Is the System identification.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult SystemToStarList(string id)
        {
            return this.View(nameof(this.ListStar), this.systemLogic.GetStar(id));
        }

        /// <summary>
        /// Creating a View for updating systems.
        /// </summary>
        /// <param name="id">Is the System identification.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult UpdateSystem(string id)
        {
            return this.View(nameof(this.UpdateSystem), this.systemLogic.GetSystem(id));
        }

        /// <summary>
        /// updates system to database.
        /// </summary>
        /// <param name="s">Is the System object reference.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpPost]
        public IActionResult UpdateSystem(Models.StarSystem s)
        {
            this.systemLogic.UpdateSystem(s.SystemID, s);

            return this.View(nameof(this.ListSystem), this.systemLogic.GetAllSystem());
        }

        /// <summary>
        /// deletes system from database.
        /// </summary>
        /// <param name="id">Is the System identification.</param>
        /// /// <returns>With IActionResult.</returns>
        public IActionResult DeleteSystem(string id)
        {
            this.systemLogic.DeleteSystem(id);
            return this.View(nameof(this.ListSystem), this.systemLogic.GetAllSystem());
        }

        // Star

        /// <summary>
        /// Creating a View for adding stars.
        /// </summary>
        /// <param name="id">Star identification.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult AddStar(string id)
        {
            return this.View(nameof(this.AddStar), id);
        }

        /// <summary>
        /// Adds star to database.
        /// </summary>
        /// <param name="s">Is the Star object reference.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpPost]
        public IActionResult AddStar(Star s)
        {
            s.StarID = Guid.NewGuid().ToString();

            this.starLogic.AddStar(s);
            return this.RedirectToAction(nameof(this.ListStar), this.systemLogic.GetStar(s.SystemID));
        }

        /// <summary>
        /// Creating a View for listing planets to stars.
        /// </summary>
        /// <param name="id">Is the System identification.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult StarToPlanetList(string id)
        {
            return this.View(nameof(this.ListPlanet), this.starLogic.GetPlanet(id));
        }

        /// <summary>
        /// Creating a View for listing stars.
        /// </summary>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult ListAllStar()
        {
            return this.View(this.starLogic.GetAllStar());
        }

        /// <summary>
        /// Creating a View for listing stars.
        /// </summary>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult ListStar()
        {
            return this.View(this.starLogic.GetAllStar());
        }

        /// <summary>
        /// Creating a View for updating stars.
        /// </summary>
        /// <param name="id">Is the Star identification.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult UpdateStar(string id)
        {
            return this.View(nameof(this.UpdateStar), this.starLogic.GetStar(id));
        }

        /// <summary>
        /// updates stars to database.
        /// </summary>
        /// <param name="s">Is the Star object reference.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpPost]
        public IActionResult UpdateStar(Star s)
        {
            this.starLogic.UpdateStar(s.StarID, s);

            return this.View(nameof(this.ListStar), this.starLogic.StarToSystem(s.SystemID));
        }

        /// <summary>
        /// deletes stars from database.
        /// </summary>
        /// <param name="id">Is the Star identification.</param>
        /// /// <returns>With IActionResult.</returns>
        public IActionResult DeleteStar(string id)
        {
            this.starLogic.DeleteStar(id);
            return this.View(nameof(this.ListStar), this.starLogic.GetAllStar());
        }

        // Planet

        /// <summary>
        /// Creating a View for adding planets.
        /// </summary>
        /// <param name="id">Planet identification.</param>
        /// /// <returns>With IActionResult.</returns>
        public IActionResult AddPlanet(string id)
        {
            return this.View(nameof(this.AddPlanet), id);
        }

        /// <summary>
        /// Adds planet to database.
        /// </summary>
        /// <param name="p">Is the Planet object reference.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpPost]
        public IActionResult AddPlanet(Planet p)
        {
            p.PlanetID = Guid.NewGuid().ToString();

            this.planetLogic.AddPlanet(p);
            return this.RedirectToAction(nameof(this.ListPlanet));
        }

        /// <summary>
        /// Creating a View for listing planets.
        /// </summary>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult ListAllPlanet()
        {
            return this.View(this.planetLogic.GetAllPlanet());
        }

        /// <summary>
        /// Creating a View for listing planets.
        /// </summary>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult ListPlanet()
        {
            return this.View(this.planetLogic.GetAllPlanet());
        }

        /// <summary>
        /// Creating a View for updating planets.
        /// </summary>
        /// <param name="id">Is the Star identification.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult UpdatePlanet(string id)
        {
            return this.View(nameof(this.UpdatePlanet), this.planetLogic.GetPlanet(id));
        }

        /// <summary>
        /// updates planets to database.
        /// </summary>
        /// <param name="p">Is the Star object reference.</param>
        /// /// <returns>With IActionResult.</returns>
        [HttpPost]
        public IActionResult UpdatePlanet(Planet p)
        {
            this.planetLogic.UpdatePlanet(p.PlanetID, p);

            return this.View(nameof(this.ListPlanet), this.planetLogic.PlanetToStar(p.StarID));
        }

        /// <summary>
        /// deletes stars from database.
        /// </summary>
        /// <param name="id">Is the Star identification.</param>
        /// /// <returns>With IActionResult.</returns>
        public IActionResult DeletePlanet(string id)
        {
            this.planetLogic.DeletePlanet(id);
            return this.View(nameof(this.ListPlanet), this.planetLogic.GetAllPlanet());
        }

        // data filler

        /// <summary>
        /// fills up the database.
        /// </summary>
        /// <returns>With IActionResult.</returns>
        [HttpGet]
        public IActionResult FillWithData()
        {
            if (filled)
            {
                // 1. system
                Models.StarSystem sys1 = new Models.StarSystem() { SystemID = Guid.NewGuid().ToString(), Name = "Illinium", SectorName = "B-123" };
                this.systemLogic.AddSystem(sys1);

                // 1. star
                Star s1 = new Star() { StarID = Guid.NewGuid().ToString(), Name = "Galo", Age = 1000000, StarType = StarType.Red_Dwarfs, SystemID = sys1.SystemID };
                this.starLogic.AddStar(s1);

                Planet p1 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Kepler-6543B2", Habitable = true, PlanetType = PlanetType.Terran, Population = 10, StarID = s1.StarID };
                this.planetLogic.AddPlanet(p1);
                Planet p2 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Branco", Habitable = false, PlanetType = PlanetType.Superterran, Population = 0, StarID = s1.StarID };
                this.planetLogic.AddPlanet(p2);
                Planet p3 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Elizabeth", Habitable = false, PlanetType = PlanetType.Jovian, Population = 0, StarID = s1.StarID };
                this.planetLogic.AddPlanet(p3);
                Planet p4 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Gemini-65422A", Habitable = false, PlanetType = PlanetType.Superterran, Population = 0, StarID = s1.StarID };
                this.planetLogic.AddPlanet(p4);
                Planet p5 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Pisces-493294", Habitable = false, PlanetType = PlanetType.Jovian, Population = 0, StarID = s1.StarID };
                this.planetLogic.AddPlanet(p5);

                // 2. star
                Star s2 = new Star() { StarID = Guid.NewGuid().ToString(), Name = "Re", Age = 1000000000, StarType = StarType.Yellow_Dwarfs, SystemID = sys1.SystemID };
                this.starLogic.AddStar(s2);
                Planet p6 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Kepler-6543B2", Habitable = true, PlanetType = PlanetType.Terran, Population = 0, StarID = s2.StarID };
                this.planetLogic.AddPlanet(p6);
                Planet p7 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Branco", Habitable = false, PlanetType = PlanetType.Superterran, Population = 0, StarID = s2.StarID };
                this.planetLogic.AddPlanet(p7);
                Planet p8 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Elizabeth", Habitable = false, PlanetType = PlanetType.Jovian, Population = 0, StarID = s2.StarID };
                this.planetLogic.AddPlanet(p8);
                Planet p9 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Gemini-65422A", Habitable = false, PlanetType = PlanetType.Superterran, Population = 0, StarID = s2.StarID };
                this.planetLogic.AddPlanet(p9);
                Planet p10 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Pisces-493294", Habitable = false, PlanetType = PlanetType.Jovian, Population = 0, StarID = s2.StarID };
                this.planetLogic.AddPlanet(p10);

                // 3. star
                Star s3 = new Star() { StarID = Guid.NewGuid().ToString(), Name = "Draco", Age = 56500000, StarType = StarType.Orange_Dwarfs, SystemID = sys1.SystemID };
                this.starLogic.AddStar(s3);

                Planet p11 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Kepler-6543B2", Habitable = true, PlanetType = PlanetType.Terran, Population = 0, StarID = s3.StarID };
                this.planetLogic.AddPlanet(p11);
                Planet p12 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Branco", Habitable = false, PlanetType = PlanetType.Superterran, Population = 0, StarID = s3.StarID };
                this.planetLogic.AddPlanet(p12);
                Planet p13 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Elizabeth", Habitable = false, PlanetType = PlanetType.Jovian, Population = 0, StarID = s3.StarID };
                this.planetLogic.AddPlanet(p13);
                Planet p14 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Gemini-65422A", Habitable = false, PlanetType = PlanetType.Superterran, Population = 0, StarID = s3.StarID };
                this.planetLogic.AddPlanet(p14);
                Planet p15 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Pisces-493294", Habitable = false, PlanetType = PlanetType.Jovian, Population = 0, StarID = s3.StarID };
                this.planetLogic.AddPlanet(p15);

                // 2. system
                Models.StarSystem sys2 = new Models.StarSystem() { SystemID = Guid.NewGuid().ToString(), Name = "Scorpius", SectorName = "B-125" };
                this.systemLogic.AddSystem(sys2);

                // 4. star sys 2
                Star s4 = new Star() { StarID = Guid.NewGuid().ToString(), Name = "Leo", Age = 1000000, StarType = StarType.Red_Dwarfs, SystemID = sys2.SystemID };
                this.starLogic.AddStar(s4);
                Planet p17 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Branco", Habitable = false, PlanetType = PlanetType.Superterran, Population = 0, StarID = s4.StarID };
                this.planetLogic.AddPlanet(p17);
                Planet p18 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Elizabeth", Habitable = false, PlanetType = PlanetType.Jovian, Population = 0, StarID = s4.StarID };
                this.planetLogic.AddPlanet(p18);
                Planet p19 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Gemini-65422A", Habitable = false, PlanetType = PlanetType.Superterran, Population = 0, StarID = s4.StarID };
                this.planetLogic.AddPlanet(p19);
                Planet p20 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Pisces-493294", Habitable = false, PlanetType = PlanetType.Jovian, Population = 0, StarID = s4.StarID };
                this.planetLogic.AddPlanet(p20);

                // 5. star sys 2
                Star s5 = new Star() { StarID = Guid.NewGuid().ToString(), Name = "Pegasus", Age = 1000000000, StarType = StarType.Yellow_Dwarfs, SystemID = sys2.SystemID };
                this.starLogic.AddStar(s5);
                Planet p21 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Kepler-6543B2", Habitable = true, PlanetType = PlanetType.Terran, Population = 0, StarID = s5.StarID };
                this.planetLogic.AddPlanet(p21);
                Planet p22 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Branco", Habitable = false, PlanetType = PlanetType.Superterran, Population = 0, StarID = s5.StarID };
                this.planetLogic.AddPlanet(p22);
                Planet p23 = new Planet() { PlanetID = Guid.NewGuid().ToString(), Name = "Elizabeth", Habitable = false, PlanetType = PlanetType.Jovian, Population = 0, StarID = s5.StarID };
                this.planetLogic.AddPlanet(p23);

                filled = false;
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        // non crud

        /// <summary>
        /// Creating a View for stats page.
        /// </summary>
        /// /// <returns>With IActionResult.</returns>
        public IActionResult Stats()
        {
            Stats s = new Stats();

            s.StarsWithLife = this.statsLogic.StarsWithLife();
            s.PopulationInSectors = this.statsLogic.PopulationInSectors();
            s.PlanetTypeGrouped = this.statsLogic.PlanetTypeGrouped();
            return this.View(s);
        }
    }
}