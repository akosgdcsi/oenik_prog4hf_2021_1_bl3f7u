﻿// <copyright file="PlanetsApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MVC.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Logic;
    using Microsoft.AspNetCore.Mvc;
    using MVC.Model;

    /// <summary>
    /// Planets api controller.
    /// </summary>
    public class PlanetsApiController : Controller
    {
        private PlanetLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlanetsApiController"/> class.
        /// </summary>
        /// <param name="logic">basic planetlogic.</param>
        /// <param name="mapper">basic mapper.</param>
        public PlanetsApiController(PlanetLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets all the planet in the db..
        /// </summary>
        /// <returns>Ienumarable planets. </returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Model.Planet> GetAll()
        {
            var planets = this.logic.GetAllPlanet();
            return this.mapper.Map<IList<Models.Planet>, List<Model.Planet>>(planets.ToList());
        }

        /// <summary>
        /// Deletes a planet.
        /// </summary>
        /// <param name="id">basic id.</param>
        /// <returns>an apiresult.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOnePlanet(string id)
        {
            if (this.logic.GetPlanet(id) != null)
            {
                this.logic.DeletePlanet(id);
                return new ApiResult() { OperationResult = true };
            }
            else
            {
                return new ApiResult() { OperationResult = false };
            }
        }

        /// <summary>
        /// adds a planet.
        /// </summary>
        /// <param name="planet">basic planet model.</param>
        /// <returns>an apiresult.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOnePlanet(Model.Planet planet)
        {
            bool success = true;
            try
            {
                Models.Planet p = new Models.Planet() { PlanetID = Guid.NewGuid().ToString(), Habitable = planet.Habitable, Population = planet.Population, Name = planet.Name };
                switch (planet.PlanetType)
                {
                    default:
                    case PlanetType.Jovian:
                        p.PlanetType = Models.PlanetType.Jovian;
                        break;

                    case PlanetType.Miniterran:
                        p.PlanetType = Models.PlanetType.Miniterran;
                        break;

                    case PlanetType.Neptunian:
                        p.PlanetType = Models.PlanetType.Neptunian;
                        break;

                    case PlanetType.Subterran:
                        p.PlanetType = Models.PlanetType.Subterran;
                        break;

                    case PlanetType.Superterran:
                        p.PlanetType = Models.PlanetType.Superterran;
                        break;

                    case PlanetType.Terran:
                        p.PlanetType = Models.PlanetType.Terran;
                        break;
                }

                this.logic.AddPlanet(p);
            }
            catch (Exception)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// mod a planet.
        /// </summary>
        /// <param name="planet">basic planet.</param>
        /// <returns>an api result.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOnePlanet(Model.Planet planet)
        {
            if (this.logic.GetPlanet(planet.PlanetID) != null)
            {
                Models.Planet p = new Models.Planet() { PlanetID = planet.PlanetID, Habitable = planet.Habitable, Population = planet.Population, Name = planet.Name };
                switch (planet.PlanetType)
                {
                    default:
                    case PlanetType.Jovian:
                        p.PlanetType = Models.PlanetType.Jovian;
                        break;

                    case PlanetType.Miniterran:
                        p.PlanetType = Models.PlanetType.Miniterran;
                        break;

                    case PlanetType.Neptunian:
                        p.PlanetType = Models.PlanetType.Neptunian;
                        break;

                    case PlanetType.Subterran:
                        p.PlanetType = Models.PlanetType.Subterran;
                        break;

                    case PlanetType.Superterran:
                        p.PlanetType = Models.PlanetType.Superterran;
                        break;

                    case PlanetType.Terran:
                        p.PlanetType = Models.PlanetType.Terran;
                        break;
                }

                this.logic.UpdatePlanet(planet.PlanetID, p);
                return new ApiResult() { OperationResult = true };
            }
            else
            {
                return new ApiResult() { OperationResult = false };
            }
        }
    }
}