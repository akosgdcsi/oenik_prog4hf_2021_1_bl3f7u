// <copyright file="Startup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MVC
{
    using AutoMapper;
    using Logic;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Models;
    using MVC.Model;
    using Repository;

    /// <summary>
    /// This is a documation for Startup file.
    /// </summary>
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        /// <summary>
        /// This is responsible for addig the transient on startup and runtime compilation.
        /// </summary>
        /// /// <param name="services">Is the IServiceCollection reference.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(opt => opt.EnableEndpointRouting = false).AddRazorRuntimeCompilation();
            services.AddTransient<PlanetLogic, PlanetLogic>();
            services.AddTransient<StarLogic, StarLogic>();
            services.AddTransient<SystemLogic, SystemLogic>();
            services.AddTransient<StatsLogic, StatsLogic>();
            services.AddTransient<IRepository<Models.Planet>, PlanetRepository>();
            services.AddTransient<IRepository<Star>, StarRepository>();
            services.AddTransient<IRepository<Models.StarSystem>, SystemRepository>();
            services.AddSingleton<IMapper>(provider => MapperFactory.CreatMapper());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.

        /// <summary>
        /// This is responsible for configuring the mvc.
        /// </summary>
        /// /// <param name="app">Is the IApplicationBuilder reference.</param>
        /// /// <param name="env">Is the IWebHostEnvironment reference.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}