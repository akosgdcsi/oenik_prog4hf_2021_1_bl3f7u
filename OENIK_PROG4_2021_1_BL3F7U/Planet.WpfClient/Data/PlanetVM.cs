﻿// <copyright file="PlanetVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Planet.WpfClient.Data
{
    using System;
    using System.Text.Json.Serialization;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Types of Planets.
    /// </summary>
    public enum PlanetType
    {
        /// <summary>
        /// Represents a Miniterran.
        /// </summary>
        Miniterran,

        /// <summary>
        /// Represents a Subterran.
        /// </summary>
        Subterran,

        /// <summary>
        /// Represents a Terran.
        /// </summary>
        Terran,

        /// <summary>
        /// Represents a Superterran.
        /// </summary>
        Superterran,

        /// <summary>
        /// Represents a Neptunian.
        /// </summary>
        Neptunian,

        /// <summary>
        /// Represents a Jovian.
        /// </summary>
        Jovian,
    }

    /// <summary>
    /// This is a documation for Planet file.
    /// </summary>
    public class PlanetVM : ObservableObject
    {
        private string planetID;
        private int population;
        private PlanetType planetType;
        private string name;
        private bool habitable;

        /// <summary>
        /// Gets or sets the identification of a planet.
        /// </summary>
        public string PlanetID
        {
            get { return this.planetID; }
            set { this.Set(ref this.planetID, value); }
        }

        /// <summary>
        /// Gets or sets the population of a planet.
        /// </summary>
        public int Population
        {
            get { return this.population; }
            set { this.Set(ref this.population, value); }
        }

        /// <summary>
        /// Gets or sets the type of a planet.
        /// </summary>
        public PlanetType PlanetType
        {
            get { return this.planetType; }
            set { this.Set(ref this.planetType, value); }
        }

        /// <summary>
        /// Gets or sets the name of a planet.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the item is enabled.
        /// </summary>
        public bool Habitable
        {
            get { return this.habitable; }
            set { this.Set(ref this.habitable, value); }
        }

        /// <summary>
        /// Gets a planet array.
        /// </summary>
        [JsonIgnore]
        public Array Planets
        {
            get { return Enum.GetValues(typeof(PlanetType)); }
        }

        /// <summary>
        /// copys a another planet vm.
        /// </summary>
        /// <param name="oth">basic planet vm.</param>
        public void Copyfrom(PlanetVM oth)
        {
            if (oth == null)
            {
                return;
            }

            this.PlanetID = oth.PlanetID;
            this.PlanetType = oth.PlanetType;
            this.Population = oth.Population;
            this.Habitable = oth.Habitable;
            this.Name = oth.Name;
        }
    }
}