﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Planet.WpfClient.VM
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using Planet.WpfClient.Data;
    using Planet.WpfClient.Logic;

    /// <summary>
    /// init a Main VM.
    /// </summary>
    internal class MainViewModel : ViewModelBase
    {
        private IMainLogic logic;
        private PlanetVM selectedPlanet;
        private ObservableCollection<PlanetVM> allPlanets;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">basic logic.</param>
        public MainViewModel(IMainLogic logic)
        {
            this.logic = logic;
            this.LoadCmd = new RelayCommand(() => this.AllPlanets = new ObservableCollection<PlanetVM>(
                this.logic.ApiGetPlanets()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelPlanets(this.SelectedPlanet));
            this.AddCmd = new RelayCommand(() => this.logic.EditPlanet(null, this.Editorfunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditPlanet(this.SelectedPlanet, this.Editorfunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
           : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets a AllPlanets.
        /// </summary>
        public ObservableCollection<PlanetVM> AllPlanets
        {
            get { return this.allPlanets; }
            set { this.Set(ref this.allPlanets, value); }
        }

        /// <summary>
        /// Gets or sets a Selected Planet.
        /// </summary>
        public PlanetVM SelectedPlanet
        {
            get { return this.selectedPlanet; }
            set { this.Set(ref this.selectedPlanet, value); }
        }

        /// <summary>
        /// Gets or sets an editor funcion.
        /// </summary>
        public Func<PlanetVM, bool> Editorfunc { get; set; }

        /// <summary>
        /// Gets or sets add command.
        /// </summary>
        public ICommand AddCmd { get; set; }

        /// <summary>
        /// Gets or sets delete command.
        /// </summary>
        public ICommand DelCmd { get; set; }

        /// <summary>
        /// Gets or sets a mod command.
        /// </summary>
        public ICommand ModCmd { get; set; }

        /// <summary>
        /// Gets or sets a load command.
        /// </summary>
        public ICommand LoadCmd { get; set; }
    }
}