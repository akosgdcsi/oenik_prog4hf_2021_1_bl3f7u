﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Planet.WpfClient.Logic
{
    using System;
    using System.Collections.Generic;
    using Planet.WpfClient.Data;

    /// <summary>
    /// init a IMainLogic.
    /// </summary>
    internal interface IMainLogic
    {
        /// <summary>
        /// Gets all the planets.
        /// </summary>
        /// <returns>List Planet VM.</returns>
        List<PlanetVM> ApiGetPlanets();

        /// <summary>
        /// Deletes a planet.
        /// </summary>
        /// <param name="planet">Planet VM.</param>
        void ApiDelPlanets(PlanetVM planet);

        /// <summary>
        /// edits a planet.
        /// </summary>
        /// <param name="planet">basic Planet VM.</param>
        /// <param name="editFunc">Func editfunction.</param>
        void EditPlanet(PlanetVM planet, Func<PlanetVM, bool> editFunc);

        /// <summary>
        /// sends a message.
        /// </summary>
        /// <param name="success"> basic bool.</param>
        void SendMessage(bool success);
    }
}