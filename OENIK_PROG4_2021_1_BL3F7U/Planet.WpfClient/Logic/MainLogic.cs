﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Planet.WpfClient.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text.Json;
    using GalaSoft.MvvmLight.Messaging;
    using Planet.WpfClient.Data;

    /// <summary>
    /// init MainLogic.
    /// </summary>
    internal class MainLogic : IMainLogic
    {
        private string url = "http://localhost:5000/PlanetsApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonSerializer = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <inheritdoc/>
        public void SendMessage(bool success)
        {
            string msg = success ? "Operation Completed Succsessfully" : "Operation Failed";
            Messenger.Default.Send(msg, "PlanetResult");
        }

        /// <inheritdoc/>
        public List<PlanetVM> ApiGetPlanets()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<PlanetVM>>(json, this.jsonSerializer);
            return list;
        }

        /// <inheritdoc/>
        public void ApiDelPlanets(PlanetVM planet)
        {
            bool success = false;
            if (planet != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + planet.PlanetID).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <inheritdoc/>
        public void EditPlanet(PlanetVM planet, Func<PlanetVM, bool> editFunc)
        {
            PlanetVM clone = new PlanetVM();
            if (planet != null)
            {
                clone.Copyfrom(planet);
            }

            bool? success = editFunc?.Invoke(clone);
            if (success == true)
            {
                if (planet != null)
                {
                    success = this.ApiEditPlanet(clone, true);
                }
                else
                {
                    success = this.ApiEditPlanet(clone, false);
                }
            }

            this.SendMessage(success == true);
        }

        private bool ApiEditPlanet(PlanetVM planet, bool isEditing)
        {
            if (planet == null)
            {
                return false;
            }

            string myurl = isEditing ? this.url + "mod" : this.url + "add";

            Dictionary<string, string> postdata = new Dictionary<string, string>();
            if (isEditing)
            {
                postdata.Add("planetID", planet.PlanetID);
            }

            postdata.Add("population", planet.Population.ToString());
            postdata.Add("habitable", planet.Habitable.ToString());
            postdata.Add("planetType", planet.PlanetType.ToString());
            postdata.Add("name", planet.Name);

            string json = this.client.PostAsync(myurl, new FormUrlEncodedContent(postdata)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }
    }
}