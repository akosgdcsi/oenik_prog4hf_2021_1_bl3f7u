﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Pending>", Scope = "member", Target = "~M:Planet.WpfClient.Logic.MainLogic.ApiGetPlanets~System.Collections.Generic.List{Planet.WpfClient.Data.PlanetVM}")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Pending>", Scope = "member", Target = "~M:Planet.WpfClient.Logic.MainLogic.ApiDelPlanets(Planet.WpfClient.Data.PlanetVM)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:Planet.WpfClient.Logic.MainLogic.ApiEditPlanet(Planet.WpfClient.Data.PlanetVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Pending>", Scope = "member", Target = "~M:Planet.WpfClient.Logic.MainLogic.ApiEditPlanet(Planet.WpfClient.Data.PlanetVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<Pending>", Scope = "type", Target = "~T:Planet.WpfClient.Logic.MainLogic")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Pending>", Scope = "member", Target = "~M:Planet.WpfClient.Logic.MainLogic.ApiEditPlanet(Planet.WpfClient.Data.PlanetVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>", Scope = "member", Target = "~P:Planet.WpfClient.Data.PlanetVM.Planets")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "Late bound")]
[assembly: System.CLSCompliant(false)]
